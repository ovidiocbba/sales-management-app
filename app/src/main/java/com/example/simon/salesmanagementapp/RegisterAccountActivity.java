package com.example.simon.salesmanagementapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.simon.salesmanagementapp.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterAccountActivity extends AppCompatActivity implements Validator.ValidationListener {


    @BindView(R.id.userNameEditText)
    @NotEmpty(messageResId = R.string.messageUserNameRequired)
    EditText userNameEditText;


    @BindView(R.id.userEmailEditText)
    @NotEmpty(messageResId =R.string.messageEmailRequired)
    @Email(messageResId = R.string.messageInvalidEmail)
    EditText userEmailEditText;

    @BindView(R.id.userPasswordEditText)
    @NotEmpty(messageResId =R.string.messagePasswordRequired)
    @Password(min = 5, scheme = Password.Scheme.ALPHA_NUMERIC_MIXED_CASE)
    EditText userPasswordEditText;

    @BindView(R.id.RepeatPasswordEditText)
    @ConfirmPassword
    EditText repeatPasswordEditText;

    /*
    @BindView(R.id.registerAccountButton)
    Button registerAccountButton;
*/
    private Validator validator;


    private ProgressDialog progressDialog;

    //defining firebaseauth object
    private FirebaseAuth mAuth;

    private DatabaseReference mDatabaseUser;

    private static final int GALLERY_REQUEST = 1;
    private Uri mImageUri = null;
    private StorageReference mStorageImage;
    private ImageButton mSetupImageButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_register_account);

        ButterKnife.bind(this);

        validator = new Validator(this);
        validator.setValidationListener(this);

        progressDialog = new ProgressDialog(this);

        //initializing firebase auth object
        mAuth = FirebaseAuth.getInstance();
        //Guardar en base de datos
        mDatabaseUser= FirebaseDatabase.getInstance().getReference().child("User");

        mStorageImage = FirebaseStorage.getInstance().getReference().child("Profile_images");

        mSetupImageButton = (ImageButton) findViewById(R.id.setupImageButton);

        mSetupImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent galleryIntent = new Intent();
                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, GALLERY_REQUEST);

            }
        });



    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("Message", "Abrir Crop Image");
        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {
            //https://github.com/ArthurHub/Android-Image-Cropper/wiki
            //Log.d("Message","Test Crop");
            Uri imageUri = data.getData();
            CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1, 1)
                    .start(this);

        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                mImageUri = result.getUri();
                mSetupImageButton.setImageURI(mImageUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }

    }

     @OnClick(R.id.registerAccountButton)
      public void registerAccountButton(){

         validator.validate();

     }


    @OnClick(R.id.backRegisterAccountButton)
    public void backRegisterAccountButton(){
        finish();
        Intent intent=new Intent(this, LoginActivity.class);
        startActivity(intent);
    }




    ///Validation
    @Override
    public void onValidationSucceeded() {
        //Toast.makeText(this, "Yay! we got it right!", Toast.LENGTH_SHORT).show();
        //creating a new user
        final String userName = userNameEditText.getText().toString().trim();
        final String email = userEmailEditText.getText().toString().trim();
        final String password = userPasswordEditText.getText().toString().trim();

        //displaying a progress dialog
        progressDialog.setMessage(getString(R.string.messageRegisteringPleaseWait));
        progressDialog.show();

        if (mImageUri != null) {

            mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    //checking if success
                    if (task.isSuccessful()) {

                        final String user_id = mAuth.getCurrentUser().getUid();
                        DatabaseReference current_user_db = mDatabaseUser.child(user_id);
                        StorageReference filepath = mStorageImage.child(mImageUri.getLastPathSegment());
                        //Save profile user
                        filepath.putFile(mImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                //Save DataBase
                                Log.d("user id recogido: ", user_id);
                                String downloadUri = taskSnapshot.getDownloadUrl().toString();
                                DatabaseReference current_user_db = mDatabaseUser.child(user_id);
                                //https://firebase.google.com/docs/database/android/save-data?hl=es
                                User user = new User(userName, email,downloadUri);
                                current_user_db.setValue(user);
                                /*
                                current_user_db.child("User_Name").setValue(userName);
                                current_user_db.child("User_Email").setValue(email);
                                current_user_db.child("Image_Url").setValue(downloadUri);
                                */



                                progressDialog.dismiss();
                                //display some message here
                                Toast.makeText(RegisterAccountActivity.this, R.string.messageSuccessfullyRegistered, Toast.LENGTH_LONG).show();
                                finish();
                                Intent mainIntent = new Intent(RegisterAccountActivity.this, NavigationDrawerActivity.class);
                                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(mainIntent);

                            }
                        });

                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(RegisterAccountActivity.this, R.string.messageRegistrationError, Toast.LENGTH_LONG).show();
                    }

                }
            });

        } else {
            progressDialog.dismiss();
            Toast.makeText(RegisterAccountActivity.this, "Error Seleccione una Imagen", Toast.LENGTH_LONG).show();

        }
    }


    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
    ///End Validation



}
