package com.example.simon.salesmanagementapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.example.simon.salesmanagementapp.model.User;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import com.mobsandgeeks.saripaar.annotation.Url;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import butterknife.BindView;
import butterknife.OnClick;

public class SetupActivity extends AppCompatActivity {

    /*
    @BindView(R.id.userNameEditText)
    @NotEmpty(messageResId = R.string.messageUserNameRequired)
    EditText userNameEditText;
*/

    private ImageButton mSetupImageButton;
    private EditText nUserNameEditText;
    private Button mSubmitButton;
    private Uri mImageUri = null;
    private StorageReference mStorageImage;

    private static final int GALLERY_REQUEST = 1;
    private DatabaseReference mDatabaseUsers;
    private FirebaseAuth mAuth;
    private ProgressDialog mProgress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);

        mStorageImage = FirebaseStorage.getInstance().getReference().child("Profile_images");
        mAuth = FirebaseAuth.getInstance();
        mDatabaseUsers = FirebaseDatabase.getInstance().getReference().child("User");

        mProgress = new ProgressDialog(this);
        mSetupImageButton = (ImageButton) findViewById(R.id.setupImageButton);
        nUserNameEditText = (EditText) findViewById(R.id.setupNameEditText);
        mSubmitButton = (Button) findViewById(R.id.setupSubmitButton);

        mSubmitButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                starSetupAccount();
            }


        });


        mSetupImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent galleryIntent = new Intent();
                galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, GALLERY_REQUEST);

            }
        });

    }

    private void starSetupAccount() {
        final String name = nUserNameEditText.getText().toString().trim();
        final String user_id = mAuth.getCurrentUser().getUid();
        final String user_email = mAuth.getCurrentUser().getEmail();
        if (!TextUtils.isEmpty(name) && mImageUri != null) {
            mProgress.setMessage(getString(R.string.messageCompletingTheConfiguration));
            mProgress.show();

            StorageReference filepath = mStorageImage.child(mImageUri.getLastPathSegment());
            filepath.putFile(mImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    //mDatabaseUsers.child(user_id);
                    String downloadUri = taskSnapshot.getDownloadUrl().toString();
                    /*
                    mDatabaseUsers.child(user_id).child("User_Name").setValue(name);
                    mDatabaseUsers.child(user_id).child("User_Email").setValue(user_email);
                    mDatabaseUsers.child(user_id).child("Image_Url").setValue(downloadUri);
                    */
                    User user = new User(name, user_email,downloadUri);
                    mDatabaseUsers.child(user_id).setValue(user);

                    mProgress.dismiss();
                    Intent mainIntent =new Intent(SetupActivity.this,NavigationDrawerActivity.class);
                    mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(mainIntent);


                }
            });


        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("Message", "Abrir Crop Image");
        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {
            //https://github.com/ArthurHub/Android-Image-Cropper/wiki
            //Log.d("Message","Test Crop");
            Uri imageUri = data.getData();
            CropImage.activity(imageUri)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1, 1)
                    .start(this);

        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                mImageUri = result.getUri();
                mSetupImageButton.setImageURI(mImageUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }

    }
    /*
    @OnClick(R.id.setupSubmitButton)
    public void setupSubmitButton(){

    }
    */
}
