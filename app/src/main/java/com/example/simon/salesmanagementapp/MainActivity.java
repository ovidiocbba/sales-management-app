package com.example.simon.salesmanagementapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences preferences = getSharedPreferences(getString(R.string.app_name),MODE_PRIVATE);
        // int user_id = preferences.getInt("user_id",0);


        String  user_id = preferences.getString("user_id","");
        if (user_id.isEmpty())
        {

            //Usuario no Logueado
            Intent intent=new Intent(this,LoginActivity.class);
            startActivity(intent);

        }
        else {

            Toast.makeText(this, "user_id: " + user_id, Toast.LENGTH_LONG).show();

        }
    }
}
