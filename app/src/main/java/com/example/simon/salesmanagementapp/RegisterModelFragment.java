package com.example.simon.salesmanagementapp;


import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.simon.salesmanagementapp.model.Model;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.api.model.StringList;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.DecimalMax;
import com.mobsandgeeks.saripaar.annotation.Digits;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class RegisterModelFragment extends Fragment implements Validator.ValidationListener {
    private static final int MAX_LENGTH = 10;


    public RegisterModelFragment() {
        // Required empty public constructor
    }

    @Length(min = 3, max = 35, messageResId = R.string.nameTooShort)
    @NotEmpty(messageResId = R.string.nameModelRequired)
    @BindView(R.id.nameModelEditText)
    EditText nameModelEditText;

    @Digits(integer = 5, fraction = 2, messageResId = R.string.shouldTwoDigit)

    @DecimalMax(value = 999999, messageResId = R.string.check_entered_value)
    @BindView(R.id.costPriceEditText)
    EditText costPriceEditText;


    @Digits(integer = 5, fraction = 2, messageResId = R.string.shouldTwoDigit)
//Should be less than max value
    @DecimalMax(value = 999999, messageResId = R.string.check_entered_value)
    @BindView(R.id.salePriceEditText)
    EditText salePriceEditText;

    @Length(min = 3, max = 50)
    @NotEmpty(messageResId = R.string.descriptionModelRequired)
    @BindView(R.id.descriptionModelEditText)
    EditText descriptionModelEditText;


    @BindView(R.id.inStockEditText)
    EditText inStockEditText;

    Validator validator;

    private ImageButton mSelectImage;
    private Uri mImageUri = null;
    private static final int GALLERY_REQUEST = 1;

    private StorageReference mStorage;
    private ProgressDialog mProgress;

    private DatabaseReference mDatabase;

    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private DatabaseReference mDatabaseUser;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mAuth = FirebaseAuth.getInstance();
        mCurrentUser = mAuth.getCurrentUser();

        /*Para que funcione las Validaciones*/
        validator = new Validator(this);
        validator.setValidationListener(this);

        // Inflate the layout for this fragment
        // return inflater.inflate(R.layout.fragment_register_model, container, false);

        View view = inflater.inflate(R.layout.fragment_register_model, container, false);
/*
         saveModelButton = (Button) view.findViewById(R.id.btnSaveModel);
         saveModelButton.setOnClickListener(new View.OnClickListener() {
         @Override
          public void onClick(View v) {
                validator.validate();
            }
        });
     */
        ButterKnife.bind(this, view);
        //Referencia donde se Guardaran las Imagenes
        mStorage = FirebaseStorage.getInstance().getReference();
        Log.d("Pagina de Subida:", mStorage.toString());
        //mDatabase = FirebaseDatabase.getInstance().getReference().child("Model");
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabaseUser = FirebaseDatabase.getInstance().getReference().child("User").child(mCurrentUser.getUid());


        mProgress = new ProgressDialog(getActivity());

        //Datos de Prueba
/*
        nameModelEditText.setText("Hollister Classic");
        costPriceEditText.setText("95");
        salePriceEditText.setText("120");
        descriptionModelEditText.setText("Jean Straight Classic With button fly Hollister");
        inStockEditText.setText("12");
*/

        mSelectImage = (ImageButton) view.findViewById(R.id.imageSelect);
        mSelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntet = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntet.setType("image/*");
                startActivityForResult(galleryIntet, GALLERY_REQUEST);
            }
        });


        return view;


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {
            mImageUri = data.getData();
            mSelectImage.setImageURI(mImageUri);
        }
    }

    @OnClick(R.id.saveModelButton)
    public void saveModelButton(View view) {

        validator.validate();
    }


    @Override
    public void onValidationSucceeded() {
        //Log.d("Validations:", "Yay! we got it right!");

        //Toast.makeText(ModelsFragment.this.getActivity(), "Yay! we got it right!", Toast.LENGTH_SHORT).show();

        //mProgress.setMessage("Upload Model.....");
        mProgress.setMessage(getString(R.string.messageRegistering));

        mProgress.show();


//file.getAbsolutePath()
//Toast.makeText(ModelsFragment.this.getActivity(),  "imagen"+mImageUri.getLastPathSegment(), Toast.LENGTH_SHORT).show();
        //final String key = mDatabase.child("Model").push().getKey();
        final String key = mDatabase.push().getKey();
        //final String nameModel = nameModelEditText.getText().toString();
        final String modelName = nameModelEditText.getText().toString();
        final Double costPrice = Double.parseDouble(costPriceEditText.getText().toString());
        final Double salePrice = Double.parseDouble(salePriceEditText.getText().toString());
        final String descriptionModel = descriptionModelEditText.getText().toString();
        final Integer inStock = Integer.parseInt(inStockEditText.getText().toString());

        if (mImageUri != null) {
            Log.d("Mi Image:", mImageUri.getLastPathSegment());
            StorageReference filepath = mStorage.child("Model_Images").child(mImageUri.getLastPathSegment());
            Log.d("Direccion Guardado", filepath.toString());

            filepath.putFile(mImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    final Uri dowloadUrl = taskSnapshot.getDownloadUrl();
                    //final DatabaseReference newModel = mDatabase.push();

                    mDatabaseUser.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {


                             /*
                            newModel.child("Model_Name").setValue(nameModel);
                            newModel.child("Cost_Price").setValue(costPrice);
                            newModel.child("Sale_Price").setValue(salePrice);
                            newModel.child("Model_Description").setValue(descriptionModel);
                            newModel.child("In_Stock").setValue(inStock);
                            newModel.child("Image_Url").setValue(dowloadUrl.toString());
                            newModel.child("User_Id").setValue(mCurrentUser.getUid());

                            */
                            final String image_Url = dowloadUrl.toString();
                            final String user_id = mCurrentUser.getUid();
                            String key = mDatabase.child("Model").push().getKey();
                            Model model = new Model(modelName, costPrice, salePrice, descriptionModel, inStock,  image_Url,  user_id);
                            Map<String, Object> modelValues = model.toMap();

                            Map<String, Object> childUpdates = new HashMap<>();
                            childUpdates.put("/Model/" + key, modelValues);
                            //childUpdates.put("/user-models/" + userId + "/" + key, modelValues);
                            mDatabase.updateChildren(childUpdates);
                            Toast.makeText(RegisterModelFragment.this.getActivity(), R.string.messageSuccessfullyRegistered, Toast.LENGTH_SHORT).show();

                            getActivity().finish();
                            Intent intent = new Intent(RegisterModelFragment.this.getActivity(), NavigationDrawerActivity.class);
                            startActivity(intent);

                            //getActivity().onBackPressed();


                            /*
                            ///Era Prueba para Obtener el Nombre de Usuario
                             newModel.child("UserName").setValue(dataSnapshot.child("User_Name").getValue()).addOnCompleteListener(new OnCompleteListener<Void>() {

                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()) {
                                        //messageSuccessfullyRegistered

                                        Toast.makeText(RegisterModelFragment.this.getActivity(), R.string.messageSuccessfullyRegistered, Toast.LENGTH_SHORT).show();

                                        //Toast.makeText(RegisterModelFragment.this.getActivity(), "Succesfully Upload Image", Toast.LENGTH_SHORT).show();
                                        //Toast.makeText(RegisterModelFragment.this.getActivity(), , Toast.LENGTH_SHORT).show();

                                        //finish();
                                        //Llamamos a la Ventana
                                        //getActivity().getFragmentManager().beginTransaction().remove(get()).commit();
                                        //getActivity().getSupportFragmentManager().popBackStack();

                                        //getActivity().getFragmentManager().beginTransaction().remove(get()).commit();
                                        getActivity().finish();
                                        Intent intent = new Intent(RegisterModelFragment.this.getActivity(), NavigationDrawerActivity.class);
                                        startActivity(intent);
                                        //Title en el Action Bar

                                    } else {

                                    }

                                }
                            });
                            */

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                    mProgress.dismiss();


                    ;
                }
            });

            // mProgress.dismiss();
        }

/*
        //UploadTask uploadTask = mountainsRef.putBytes(data);
       // Uri file = Uri.fromFile(new File("path/to/images/rivers.jpg"));
        Log.d("my Image:", mImageUri.getLastPathSegment());
        StorageReference riversRef = mStorage.child("images/"+mImageUri.getLastPathSegment());
        UploadTask uploadTask = riversRef.putFile(mImageUri);

// Register observers to listen for when the download is done or if it fails
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
               // mProgress.show();
                taskSnapshot.getMetadata();// contains file metadata such as size, content-type, and download URL.
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                mProgress.dismiss();
            }
        });


        */


        //Toast.makeText(ModelsFragment.this.getActivity(), nameModel + costPrice + salePrice + descriptionModel, Toast.LENGTH_LONG).show();
    }


    public static String random() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(MAX_LENGTH);
        char tempChar;
        for (int i = 0; i < randomLength; i++) {
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }

    //R.string.toastCheckFields
    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(RegisterModelFragment.this.getActivity());

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(RegisterModelFragment.this.getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }

    }
}
