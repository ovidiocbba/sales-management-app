package com.example.simon.salesmanagementapp.model;

/**
 * Created by Simon on 18/11/2016.
 */

public class User {
    private String User_Name;
    private String User_Email;
    private String User_Image_Url;

    public User() {
    }

    public User(String user_Name, String user_Email, String user_Image_Url) {
        User_Name = user_Name;
        User_Email = user_Email;
        User_Image_Url = user_Image_Url;
    }

    public String getUser_Name() {
        return User_Name;
    }

    public void setUser_Name(String user_Name) {
        User_Name = user_Name;
    }

    public String getUser_Email() {
        return User_Email;
    }

    public void setUser_Email(String user_Email) {
        User_Email = user_Email;
    }

    public String getUser_Image_Url() {
        return User_Image_Url;
    }

    public void setUser_Image_Url(String user_Image_Url) {
        User_Image_Url = user_Image_Url;
    }
}
