package com.example.simon.salesmanagementapp.model;

/**
 * Created by Simon on 11/02/2017.
 */

public class Detail_Sale {

    public String model_Id;
    public Integer quality;
    public Double price_unit;
    public Double amount;
    public String sale_Id;

    public Detail_Sale() {
        // Default constructor required for calls to DataSnapshot.getValue(Detail_Sale.class)
    }

    public Detail_Sale(String model_Id, Integer quality, Double price_unit, Double amount, String sale_Id) {
        this.model_Id = model_Id;
        this.quality = quality;
        this.price_unit = price_unit;
        this.amount = amount;
        this.sale_Id = sale_Id;
    }

    public String getModel_Id() {
        return model_Id;
    }

    public void setModel_Id(String model_Id) {
        this.model_Id = model_Id;
    }

    public Integer getQuality() {
        return quality;
    }

    public void setQuality(Integer quality) {
        this.quality = quality;
    }

    public Double getPrice_unit() {
        return price_unit;
    }

    public void setPrice_unit(Double price_unit) {
        this.price_unit = price_unit;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getSale_Id() {
        return sale_Id;
    }

    public void setSale_Id(String sale_Id) {
        this.sale_Id = sale_Id;
    }
}
