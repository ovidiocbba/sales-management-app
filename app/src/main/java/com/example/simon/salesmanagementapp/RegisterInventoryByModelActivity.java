package com.example.simon.salesmanagementapp;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.simon.salesmanagementapp.model.Income_Inventory;
import com.example.simon.salesmanagementapp.model.Model;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class RegisterInventoryByModelActivity extends AppCompatActivity {

    private String mModel_key = null;
    private String model_UserId;
    private TextView mModelSingleName;
    private TextView mModelSingleCurrentStock;
    Calendar dateTime = Calendar.getInstance();
    private TextView mTextViedDate;
    private ImageButton btn_date;

    private ProgressDialog mProgress;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private DatabaseReference mDatabaseUser;
    private DatabaseReference mDatabaseRegisterInvetary;

    private String modelName = null;
    private Double costPrice;
    private Double salePrice;
    private String descriptionModel;
    private Integer inStock;
    private String image_Url;

    private EditText income_in_Stock;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_inventory_by_model);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.lblIncomeInventory);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Model");
        //Recibe el Parametro Enviado
        mModel_key = getIntent().getExtras().getString("Model_Key");

        mModelSingleName = (TextView) findViewById(R.id.model_name);
        mModelSingleCurrentStock = (TextView) findViewById(R.id.model_current_stock);

        mTextViedDate = (TextView) findViewById(R.id.txt_date_time);
        btn_date = (ImageButton) findViewById(R.id.dateSelectedButton);

        income_in_Stock=(EditText)findViewById(R.id.icomeInStockEditText);

        updateTextLabel();
        DateFormat formatDateTime = DateFormat.getDateTimeInstance();


        mCurrentUser = mAuth.getCurrentUser();
        mDatabaseUser = FirebaseDatabase.getInstance().getReference().child("User").child(mCurrentUser.getUid());


        mProgress = new ProgressDialog(this);


        mDatabase.child(mModel_key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                System.out.println(dataSnapshot.getValue());
                Model model = dataSnapshot.getValue(Model.class);
                //https://firebase.google.com/docs/database/admin/retrieve-data?hl=en
                if (model != null) {
                    System.out.println("Datos de la BD");
                    System.out.println(model.getModel_Name());

                    model_UserId = model.getUser_Id();
                    modelName = model.getModel_Name();
                    costPrice = model.getCost_Price();
                    salePrice = model.getSale_Price();
                    descriptionModel = model.getModel_Description();
                    inStock = Integer.parseInt(model.getIn_Stock().toString());
                    image_Url = model.getImage_Url();

                    mModelSingleName.setText(modelName);
                    mModelSingleCurrentStock.setText(inStock.toString() + ".-");
                }

                //if(mAuth.getCurrentUser().getUid()== model_UserId)
                if (mAuth.getCurrentUser().getUid().equals(model_UserId)) {
                    // mSingleModelDeleteBtn.setVisibility(View.VISIBLE);

                }
                btn_date.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        updateDate();
                    }
                });
             }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());
            }

        });
    }

    private void updateDate() {
        new DatePickerDialog(this, d, dateTime.get(Calendar.YEAR), dateTime.get(Calendar.MONTH), dateTime.get(Calendar.DAY_OF_MONTH)).show();
    }


    DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            dateTime.set(Calendar.YEAR, year);
            dateTime.set(Calendar.MONTH, monthOfYear);
            dateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateTextLabel();
        }
    };


    private void updateTextLabel() {
        //Calendar c = Calendar.getInstance();
        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");//En Sismte
        //SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        //SimpleDateFormat sdf = new SimpleDateFormat("dd- MMMM- yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String strDate = sdf.format(dateTime.getTime());
        //mTextViedDate.setText(dateTime.getTime().toString());
        mTextViedDate.setText(strDate.toString());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.navigation_drawer, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //onBackPressed();

        int id = item.getItemId();


        if (id == android.R.id.home) {
            onBackPressed();
        }


        return super.onOptionsItemSelected(item);

    }



    public void btn_Add_Invetory(View view) {
        //Log.d("Validations:", "Yay! we got it right!");
        //Toast.makeText(ModelsFragment.this.getActivity(), "Yay! we got it right!", Toast.LENGTH_SHORT).show();
        //mProgress.setMessage("Upload Model.....");
        //messageProgressDialog();

        //mProgress.setMessage(getString(R.string.messageRegistering));
        //mProgress.show();

        mModel_key = getIntent().getExtras().getString("Model_Key");
        //mDatabase = FirebaseDatabase.getInstance().getReference().child("Model");
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabaseRegisterInvetary = FirebaseDatabase.getInstance().getReference("Income_Inventory");


        final Integer amount_entered=  Integer.parseInt(income_in_Stock.getText().toString());
        final Integer New_stock = inStock + amount_entered;
        final String income_date = mTextViedDate.getText().toString();

        mDatabaseUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                final String user_id = mCurrentUser.getUid();


                Model model = new Model(modelName, costPrice, salePrice, descriptionModel, New_stock, image_Url, user_id);
                Map<String, Object> modelValues = model.toMap();
                Map<String, Object> childUpdates = new HashMap<>();
                childUpdates.put("/Model/" + mModel_key, modelValues);

                //childUpdates.put("/Inventory_Models/" +key_Inventary + "/" + user_id, modelValues);
                //childUpdates.put("/Inventory_By_Models/" +key_Inventary + "/" + user_id, Income_stock);
                mDatabase.updateChildren(childUpdates);

                final String key_Inventary = mDatabaseRegisterInvetary.push().getKey();
                Income_Inventory income_Inventary = new Income_Inventory(income_date, amount_entered, mModel_key, user_id);
                mDatabaseRegisterInvetary.child(key_Inventary).setValue(income_Inventary);
                /*
                DatabaseReference current_inventary = mDatabaseRegisterInvetary.child(key_Inventary);
                current_inventary.child("Amount_Entered").setValue(Amount_Entered);
                current_inventary.child("Income_Date").setValue(Income_date);
                current_inventary.child("Model_Id").setValue(mModel_key);
                current_inventary.child("User_Id").setValue(user_id);
                */

                 /*
                Toast.makeText(RegisterInventoryByModelActivity.this, R.string.messageSuccessfullyRegistered, Toast.LENGTH_SHORT).show();
                RegisterInventoryByModelActivity.this.finish();

                Intent intent = new Intent(RegisterInventoryByModelActivity.this, NavigationDrawerActivity.class);
                startActivity(intent);
                */
                messageProgressDialog();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
       // mProgress.dismiss();


    }

    public  void messageProgressDialog()
    {

        //http://stackoverflow.com/questions/14445745/android-close-dialog-after-5-seconds
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.messageUpdating));
        dialog.show();
        final Timer timer2 = new Timer();
        timer2.schedule(new TimerTask() {
            public void run() {
                dialog.dismiss();
                timer2.cancel(); //this will cancel the timer of the system
                RegisterInventoryByModelActivity.this.finish();
                Intent intent = new Intent(RegisterInventoryByModelActivity.this, NavigationDrawerActivity.class);
                startActivity(intent);

            }
        }, 2000); // the timer will count 2 seconds....
    }
}

