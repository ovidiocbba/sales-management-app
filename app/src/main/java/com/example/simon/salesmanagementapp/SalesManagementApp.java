package com.example.simon.salesmanagementapp;

import android.app.Application;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

/**
 * Created by Simon on 31/10/2016.
 */

public class SalesManagementApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        //Enabling offline mode
        //FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        //http://stackoverflow.com/questions/37346363/java-lang-illegalstateexception-firebaseapp-with-name-default
        if(!FirebaseApp.getApps(this).isEmpty())
        {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        }
        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttpDownloader(this,Integer.MAX_VALUE));
        Picasso built = builder.build();
        //built.setIndicatorsEnabled(true);
        built.setIndicatorsEnabled(false);
        built.setLoggingEnabled(true);
        Picasso.setSingletonInstance(built);

    }
}
