package com.example.simon.salesmanagementapp;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.simon.salesmanagementapp.model.Model;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;


/**
 * A simple {@link Fragment} subclass.
 */
public class ModelsFragment extends Fragment{


    private RecyclerView mModelList;
    private DatabaseReference mDatabase;
    private DatabaseReference mDatabaseUsers;

    private DatabaseReference mDatabaseCurrenteUser;
    private Query mQueryCurrentUser;
    private FirebaseAuth mAuth;


    public ModelsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Model");
        mDatabaseUsers = FirebaseDatabase.getInstance().getReference().child("User");

        //For activate offline mode
        mDatabase.keepSynced(true);
        mDatabaseUsers.keepSynced(true);

        mAuth =FirebaseAuth.getInstance();
        String currentUserId= mAuth.getCurrentUser().getUid();
        mDatabaseCurrenteUser = FirebaseDatabase.getInstance().getReference().child("Model");
        ///Consulta
        mQueryCurrentUser = mDatabaseCurrenteUser.orderByChild("User_Id").equalTo(currentUserId);

        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_models, container, false);
        View view = inflater.inflate(R.layout.fragment_models, container, false);

        mModelList =(RecyclerView)view.findViewById(R.id.model_list);
        mModelList.setHasFixedSize(true);
        mModelList.setLayoutManager(new LinearLayoutManager(this.getActivity()));

        return view;

    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseRecyclerAdapter<Model,ModelViewHolder> firebaseRecyclerAdapter =new FirebaseRecyclerAdapter<Model, ModelViewHolder>(
                Model.class,
                R.layout.model_row,
                ModelViewHolder.class,
                mQueryCurrentUser
                //mDatabase

        ) {
            @Override
            protected void populateViewHolder(ModelViewHolder viewHolder, Model model, int position) {

                //Get Model ID / Key
                if(model!= null) {
                    final String model_key = getRef(position).getKey();
                    viewHolder.setModelName(model.getModel_Name());
                    viewHolder.setPriceSale(model.getSale_Price());
                    //viewHolder.setImageUrl(getApplicationContext(),model.getImage_Url());
                    viewHolder.setImageUrl(getContext(), model.getImage_Url());
                    //Escuhar evento Click
                    viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Get Model ID / Key
                            //Toast.makeText(getContext(),"Model_Key"+model_key,Toast.LENGTH_SHORT).show();
                            Intent singleModelIntent = new Intent(getContext(), ModelSingleActivity.class);
                            singleModelIntent.putExtra("Model_Key", model_key);
                            startActivity(singleModelIntent);


                        }
                    });
                }

            }
        };
        mModelList.setAdapter(firebaseRecyclerAdapter);


    }
    public static class ModelViewHolder extends RecyclerView.ViewHolder {

        View mView;
        /*
        TextView model_Model_Name;
        */

        public ModelViewHolder(View itemView) {
            super(itemView);
           // itemView= mView;
            mView= itemView;

            //Escuchara cuando se presione encima del nombre del modelo
            /*
            model_Model_Name= (TextView)mView.findViewById(R.id.model_name);
            model_Model_Name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //LoginActivity
                    Log.v("ModelActivity","Some Text");
                }
            });
            */


        }
        //Se Cargara al Row_Model
        public void setModelName(String Model_Name){
            TextView model_Model_Name= (TextView)mView.findViewById(R.id.model_name);
            model_Model_Name.setText(Model_Name);
        }
        public void setPriceSale(Double Sale_Price){
            TextView model_PriceSale= (TextView)mView.findViewById(R.id.model_price_sale);
            model_PriceSale.setText(Sale_Price.toString()+".-");
        }
        public void setImageUrl(final Context ctx, final String Image_Url){
            final ImageView model_Image_Url=(ImageView)mView.findViewById(R.id.model_image);
            //Picasso.with(ctx).load(Image_Url).into(model_Image_Url);
             Picasso.with(ctx).load(Image_Url).networkPolicy(NetworkPolicy.OFFLINE).into(model_Image_Url, new Callback() {
                 @Override
                 public void onSuccess() {

                 }

                 @Override
                 public void onError() {
                     Picasso.with(ctx).load(Image_Url).into(model_Image_Url);

                 }
             });

        }
    }
}
