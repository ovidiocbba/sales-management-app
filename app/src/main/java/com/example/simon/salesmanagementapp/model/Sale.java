package com.example.simon.salesmanagementapp.model;

/**
 * Created by Simon on 11/02/2017.
 */

public class Sale {
    public String sale_date;
    public Double total_amount;//Total Venta
    public Double received_cash; // Dinero Recibido
    public Double change;//Cambio
    public String user_Id;

    public Sale() {
        // Default constructor required for calls to DataSnapshot.getValue(Sale.class)
    }

    //Generate
    //Alt+Ins

    public Sale(String sale_date, Double total_amount, Double received_cash, Double change, String user_Id) {
        this.sale_date = sale_date;
        this.total_amount = total_amount;
        this.received_cash = received_cash;
        this.change = change;
        this.user_Id = user_Id;
    }

    public String getSale_date() {
        return sale_date;
    }

    public void setSale_date(String sale_date) {
        this.sale_date = sale_date;
    }

    public Double getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(Double total_amount) {
        this.total_amount = total_amount;
    }

    public Double getReceived_cash() {
        return received_cash;
    }

    public void setReceived_cash(Double received_cash) {
        this.received_cash = received_cash;
    }

    public Double getChange() {
        return change;
    }

    public void setChange(Double change) {
        this.change = change;
    }

    public String getUser_Id() {
        return user_Id;
    }

    public void setUser_Id(String user_Id) {
        this.user_Id = user_Id;
    }
}
