package com.example.simon.salesmanagementapp.model;

/**
 * Created by Simon on 29/11/2016.
 */

public class Income_Inventory {

    public String income_Date;
    public Integer amount_entered;
    public String model_Id;
    public String user_Id;

    public Income_Inventory() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public Income_Inventory(String income_Date, Integer amount_entered, String model_Id, String user_Id) {
        this.income_Date = income_Date;
        this.amount_entered = amount_entered;
        this.model_Id = model_Id;
        this.user_Id = user_Id;
    }

    public String getIncome_Date() {
        return income_Date;
    }

    public void setIncome_Date(String income_Date) {
        this.income_Date = income_Date;
    }

    public Integer getAmount_entered() {
        return amount_entered;
    }

    public void setAmount_entered(Integer amount_entered) {
        this.amount_entered = amount_entered;
    }

    public String getModel_Id() {
        return model_Id;
    }

    public void setModel_Id(String model_Id) {
        this.model_Id = model_Id;
    }

    public String getUser_Id() {
        return user_Id;
    }

    public void setUser_Id(String user_Id) {
        this.user_Id = user_Id;
    }
}