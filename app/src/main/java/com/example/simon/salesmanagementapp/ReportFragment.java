package com.example.simon.salesmanagementapp;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.simon.salesmanagementapp.model.Model;
import com.example.simon.salesmanagementapp.model.Sale;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


public class ReportFragment extends Fragment {

    private FirebaseAuth mAuth;
    private String currentUserId;
    private DatabaseReference mDatabase;
    private Integer cantVentas=0;
    private Integer stockDisponible=0;
    private Double sumDineroRecibido=0.0;
    private Double valorInventarioNeto=0.0;
    private Integer productoBajaStock=0;
    private Double valorVentaInventario=0.0;

    public ReportFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
         final View view = inflater.inflate(R.layout.fragment_report, container, false);

        //Probando con datos del usuario
        mAuth = FirebaseAuth.getInstance();
        //final String currentUserId= mAuth.getCurrentUser().getUid();
        currentUserId= mAuth.getCurrentUser().getUid();
        Log.i("userID:::", currentUserId);

        mDatabase = FirebaseDatabase.getInstance().getReference();
        Query mQueryCurrentUser = mDatabase.child("Sale").orderByChild("user_Id").equalTo(currentUserId);
        // LLenar Spinerrr
        //http://stackoverflow.com/questions/38492827/how-to-get-a-string-list-from-firebase-to-fill-a-spinner
        mQueryCurrentUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                cantVentas=0;
                sumDineroRecibido=0.0;
                for (DataSnapshot saleSearchSnapshot: dataSnapshot.getChildren()) {
                    System.out.println(dataSnapshot.getValue());
                    Sale currentSale = saleSearchSnapshot.getValue(Sale.class);
                    //String saleId = saleSearchSnapshot.getKey().toString();
                    // Log.i("Id:::", saleId);
                    //Dinero Cobrado
                    Double Recibido=currentSale.getTotal_amount();
                    sumDineroRecibido= sumDineroRecibido+Recibido;
                    cantVentas++;
                }
                Log.i("Reporte Global Anual:::", "***************");
                Log.i("Total Ventas:::", cantVentas.toString());
                Log.i("Total Dinero Recibido::", sumDineroRecibido.toString());
                Log.i("********************", "***************");

                TextView totalVentaTextView = (TextView)view.findViewById(R.id.cantVentasTextView);
                TextView ingresosPorVentasTextView = (TextView)view.findViewById(R.id.ingresosPorVentasTextView);
                totalVentaTextView.setText(cantVentas.toString());
                ingresosPorVentasTextView.setText(sumDineroRecibido.toString());

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

        Query queryTotalStockUserID = mDatabase.child("Model").orderByChild("User_Id").equalTo(currentUserId);
        queryTotalStockUserID.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                stockDisponible=0;
                valorInventarioNeto=0.0;
                productoBajaStock=0;
                valorVentaInventario=0.0;

                for (DataSnapshot  SearchSnapshot: dataSnapshot.getChildren()) {
                    System.out.println(dataSnapshot.getValue());
                    Model modelData = SearchSnapshot.getValue(Model.class);
                    stockDisponible=stockDisponible+modelData.getIn_Stock();


                    valorInventarioNeto=valorInventarioNeto+(modelData.getCost_Price())*(modelData.getIn_Stock());
                    valorVentaInventario=valorVentaInventario+(modelData.getSale_Price())*(modelData.getIn_Stock());
                    if(modelData.getIn_Stock()==0)
                    {
                        productoBajaStock++;
                    }

                }
                Log.i("Reporte de Inventario:", "***************");
                Log.i("Prendas Disponibles:", stockDisponible.toString());
                Log.i("Modelo Baja cantidad:", productoBajaStock.toString());

                Log.i("Valor Neto Inventario:", valorInventarioNeto.toString());
                Log.i("Valor Venta Inventario:", valorVentaInventario.toString());
                Double ValVentas=valorVentaInventario-valorInventarioNeto;
                Log.i("Ganancia Esperada:", ValVentas.toString());

                Log.i("********************", "***************");

                TextView prendDispTextView = (TextView)view.findViewById(R.id.prendasDisponiblesTextView);
                TextView valNetInvetarioTextView = (TextView)view.findViewById(R.id.valorNetoInventarioTextView);

                TextView bajoStockTextView = (TextView)view.findViewById(R.id.bajaCantidadTextView);
                TextView valFinalTextView = (TextView)view.findViewById(R.id.valorFinalVentaInventarioTextView);


                TextView ganEsperadaTextView = (TextView)view.findViewById(R.id.gananciaEsperadaTextView);

                prendDispTextView.setText(stockDisponible.toString());
                valNetInvetarioTextView.setText(valorInventarioNeto.toString());
                bajoStockTextView.setText(productoBajaStock.toString());
                valFinalTextView.setText(valorVentaInventario.toString());
                ganEsperadaTextView.setText(ValVentas.toString());



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });


        return view;


    } ;



}
