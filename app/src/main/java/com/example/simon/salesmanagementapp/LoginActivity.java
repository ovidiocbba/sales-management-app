package com.example.simon.salesmanagementapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.Builder;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements Validator.ValidationListener{



    //Obtener referencia a la raiz//Para Tener acceso a la raiz para luego poder acceder a las ramas hijas
    DatabaseReference ref= FirebaseDatabase.getInstance().getReference();
    //como mensaje es hija directa de la raiz usaremos el metodo child para obtener la referencia de la misma
    DatabaseReference mensaeRef=ref.child("mensaje");
    //Prueba FireBase
    @BindView(R.id.mesajeTextView)
    TextView mensajeTextView;


    @BindView(R.id.userEmailEditText)
    @NotEmpty(messageResId =R.string.messageEmailRequired)
    @Email(messageResId = R.string.messageInvalidEmail)
    EditText userEmailEditText;

    @BindView(R.id.userPasswordEditText)
    @NotEmpty(messageResId =R.string.messagePasswordRequired)
    EditText userPasswordEditText;


    private Validator validator;

    private ProgressDialog progressDialog;
    //defining firebaseauth object
    private FirebaseAuth mAuth;
    //private FirebaseAuth.AuthStateListener mAuthListener;
    private ProgressDialog mProgress;

    private DatabaseReference mDatabaseUser;

    private SignInButton signInButton;
    private static final int RC_SIGN_IN = 1;

    private GoogleApiClient mGoogleApiClient;
    private static final String TAG = "Login Activiyt";
    private FirebaseAuth.AuthStateListener mAuthListener;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
//        getSupportActionBar().hide();
        setContentView(R.layout.activity_login);
//        getActionBar().hide();
        getSupportActionBar().hide();

        ButterKnife.bind(this);

        validator = new Validator(this);
        validator.setValidationListener(this);


        userEmailEditText.setText("ovidiocbba@hotmail.com");
        userPasswordEditText.setText("123456Prueba");

        //initializing firebase auth object
        mAuth = FirebaseAuth.getInstance();
        /*
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

            }
        };
        */

        progressDialog = new ProgressDialog(this);
        mDatabaseUser = FirebaseDatabase.getInstance().getReference().child("User");
        //Enable Offline
        mDatabaseUser.keepSynced(true);
        Log.d("FirebaseDatabase: ",mDatabaseUser.toString());
        mProgress = new ProgressDialog(this);

        //----GOOGLE SIGN IN ----
        // Configure Google Sign In
        //String Value=getString(R.string.default_web_client_id);
        Log.d("MyToken",getString(R.string.default_web_client_id));
        Log.d("Pasando", "Por en Oncreate");

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                //.enableAutoManage(this, this)

                .enableAutoManage(this, new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

                        Toast.makeText(LoginActivity.this,"You got an Error",Toast.LENGTH_LONG).show();

                    }
                })

                .addApi(Auth.GOOGLE_SIGN_IN_API,gso)
                .build();

        /*
        signInButton.setOnClickListener(new View.OnClickListener()
        {

            @Override
            public void onClick(View v) {
                Log.d("Precionando", "Click in Sign In");

                signIn();
            }
        });
        */

         signInButton = (SignInButton) findViewById(R.id.googleSignInButton);
         for (int i = 0; i < signInButton.getChildCount(); i++) {
            View v = signInButton.getChildAt(i);

            if (v instanceof TextView) {
                TextView tv = (TextView) v;
                tv.setPadding(0, 0, 20, 0);
                return;
            }
        }





        /*
       //Esto es necesario solo en el Navigation Activity
        mAuthListener = new FirebaseAuth.AuthStateListener(){

            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    //Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());

                    //that means user is already logged in
                    //so close this activity
                    finish();
                    Log.d("Direccion Guardado", "Aqui Puede ser el error");
                    //and open profile activity
                    startActivity(new Intent(getApplicationContext(), NavigationDrawerActivity.class));
                } else {
                    // User is signed out
                    //Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ..

            }
        };
        */
/*
        //if getCurrentUser does not returns null
        if(mAuth.getCurrentUser() != null){
            //that means user is already logged in
            //so close this activity
            finish();

            //and open profile activity
             startActivity(new Intent(getApplicationContext(), NavigationDrawerActivity.class));
        }
*/


    }
    private void signIn() {
        Log.d("Entrando", " in Sign In");
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @OnClick(R.id.googleSignInButton)
    public void googleSignInButton(View view) {
        Log.d("Precionando", "Click in Sign In");
        signIn();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            mProgress.setMessage(getString(R.string.messageStartingSignIn));
            mProgress.show();
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
               // mProgress.dismiss();
                //Toast.makeText(this,"OK Sign ",Toast.LENGTH_SHORT).show();

            } else {
                // Google Sign In failed, update UI appropriately
                // ...
                mProgress.dismiss();
                Toast.makeText(this,R.string.lblLoginFailed,Toast.LENGTH_SHORT).show();
            }
        }
    }
    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {
        //Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        Log.d(TAG, "firebaseAuthWithGoogle:" + account.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            //Toast.makeText(GoogleSignInActivity.this, "Authentication failed.",
                            Toast.makeText(LoginActivity.this, "Authentication failed.",

                                            Toast.LENGTH_SHORT).show();
                        }
                        else {
                            mProgress.dismiss();
                            checkUserExist();

                            /*
                            finish();
                            Intent mainIntent=new Intent(LoginActivity.this,NavigationDrawerActivity.class);
                            mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(mainIntent);
                            Toast.makeText(LoginActivity.this, R.string.lblLoginSuccesful,Toast.LENGTH_LONG).show();
                             */
                        }
                        //mProgress.show();
                        // ...

                    }
                });
    }
    /*
    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
        // ...
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
        // ...
    }
    */

    /*
Para obtener actualizaciones de cuando se modificar un valor en nuestra bd en tiempo real
podemos asignarle un oyente a la referencia necesaria.
Eh internamente tenemos 2 metodos
Se asigna el metodo oyente dentro del ciclo onStart
Prueba para Firebase
*/
/*
    @Override
    protected void onStart() {
        super.onStart();

        mensaeRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //Obtenemos el valor de la BD
                String value = dataSnapshot.getValue(String.class);
                //Y lo colocamos en el View
                mensajeTextView.setText(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    */


    @OnClick(R.id.loginButton)
    public void loginButton(View view) {
        validator.validate();
    }

    @OnClick(R.id.registerAccountButton)
    public void registerAccountButton(View view) {
        finish();
        //Llamamos a la Ventana Dashboard
        Intent intent=new Intent(this, RegisterAccountActivity.class);
        startActivity(intent);
    }


    ///Validation
    @Override
    public void onValidationSucceeded() {

        //Toast.makeText(this, "Yay! we got it right!", Toast.LENGTH_SHORT).show();

        /*Register Generico
        String rightUserName="admin@salesmanagement.com";
        String rightPassword="123root";

        if (rightUserName.equals(userEmailEditText.getText().toString())&& rightPassword.equals(userPasswordEditText.getText().toString()))
        {
            //Login Ok
            //Contexto componente en Android
            //El contexto Soy Yo (this)
            Toast.makeText(this, R.string.lblLoginSuccesful,Toast.LENGTH_LONG).show();

            //Guardando en SharedPreferences   //Solo Permite Datos Primitivos
            SharedPreferences preferences=getSharedPreferences(getString(R.string.app_name),//Nombre Archivo
                    Context.MODE_PRIVATE);//Solo Nuestra App Puede Ver Esto
            SharedPreferences.Editor editor=preferences.edit();//Abrir el archivo para escritura
            editor.putString("user_id",userEmailEditText.getText().toString());
            editor.commit();//Guarda Cambios

            //Llamamos a la Ventana Dashboard
            finish();
            Intent intent=new Intent(this, NavigationDrawerActivity.class);
            startActivity(intent);
        }
        else {
            Toast.makeText(this, R.string.lblLoginFailed,Toast.LENGTH_LONG).show();
        }
        */
        //if the email and password are not empty
        //displaying a progress dialog

        progressDialog.setMessage(getString(R.string.messageVerifyingEnterData));
        progressDialog.show();

        //getting email and password from edit texts
        String email = userEmailEditText.getText().toString().trim();
        String password  = userPasswordEditText.getText().toString().trim();

        //logging in the user
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        //if the task is successfull
                        if(task.isSuccessful()){
                            progressDialog.dismiss();
                             //checkUserExist();
                             finish();
                             Toast.makeText(LoginActivity.this, R.string.lblLoginSuccesful,Toast.LENGTH_LONG).show();
                             startActivity(new Intent(getApplicationContext(), NavigationDrawerActivity.class));





                        }else{
                            progressDialog.dismiss();
                            //display some message here
                            Toast.makeText(LoginActivity.this, R.string.lblLoginFailed,Toast.LENGTH_LONG).show();

                        }

                    }
                });


    }

    private void checkUserExist() {

        if (mAuth.getCurrentUser() != null)
        {

            final String user_id = mAuth.getCurrentUser().getUid();

        mDatabaseUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(user_id)) {
                    /*
                   Intent mainIntent=new Intent(LoginActivity.this, NavigationDrawerActivity.class);
                   startActivity(mainIntent);
                   */
                    finish();
                    Intent mainIntent = new Intent(LoginActivity.this, NavigationDrawerActivity.class);
                    mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(mainIntent);
                    //Toast.makeText(LoginActivity.this, R.string.lblLoginSuccesful, Toast.LENGTH_LONG).show();

                } else {
                    Toast.makeText(LoginActivity.this, R.string.toastYouNeedToSetupYourAccount, Toast.LENGTH_SHORT).show();
                    Intent setupIntent = new Intent(LoginActivity.this, SetupActivity.class);
                    setupIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(setupIntent);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}


    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);
            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
    ///End Validation
}
