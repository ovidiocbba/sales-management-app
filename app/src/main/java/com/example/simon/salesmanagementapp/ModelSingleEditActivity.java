package com.example.simon.salesmanagementapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;

import android.widget.Toast;

import com.example.simon.salesmanagementapp.model.Model;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.DecimalMax;
import com.mobsandgeeks.saripaar.annotation.Digits;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ModelSingleEditActivity extends AppCompatActivity implements Validator.ValidationListener {

    private String mModel_key = null;
    //private DatabaseReference mDatabase;

    private static final int MAX_LENGTH = 10;


    @Length(min = 3, max = 35, messageResId = R.string.nameTooShort)
    @NotEmpty(messageResId = R.string.nameModelRequired)

    @BindView(R.id.nameModelEditText)
    EditText nameModelEditText;

    @Digits(integer = 5, fraction = 2, messageResId = R.string.shouldTwoDigit)

    @DecimalMax(value = 999999, messageResId = R.string.check_entered_value)
    @BindView(R.id.costPriceEditText)
    EditText costPriceEditText;


    @Digits(integer = 5, fraction = 2, messageResId = R.string.shouldTwoDigit)
//Should be less than max value
    @DecimalMax(value = 999999, messageResId = R.string.check_entered_value)
    @BindView(R.id.salePriceEditText)
    EditText salePriceEditText;

    @Length(min = 3, max = 50)
    @NotEmpty(messageResId = R.string.descriptionModelRequired)
    @BindView(R.id.descriptionModelEditText)
    EditText descriptionModelEditText;


    @BindView(R.id.inStockEditText)
    EditText inStockEditText;


    Validator validator;

    private Uri mImageUri = null;
    //urlCurrentImage
    private Uri url_Current_Image = null;

    private static final int GALLERY_REQUEST = 1;

    private StorageReference mStorage;
    private ProgressDialog mProgress;

    private DatabaseReference mDatabase;

    private FirebaseAuth mAuth;
    private FirebaseUser mCurrentUser;
    private DatabaseReference mDatabaseUser;
    private DatabaseReference mDatabaseRegister;

    private ImageButton mModelSingleImageSelected;
    private boolean state=false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.getBarEditModel);
        mAuth = FirebaseAuth.getInstance();
        mCurrentUser = mAuth.getCurrentUser();
        /*Para que funcione las Validaciones*/
        validator = new Validator(this);
        validator.setValidationListener(this);
        //ButterKnife.bind(this, view);
        //ButterKnife.bind(this,ModelSingleEditActivity.this);

        //Referencia donde se Guardaran las Imagenes
        mStorage = FirebaseStorage.getInstance().getReference();
        Log.d("Pagina de Subida:", mStorage.toString());
        //mDatabase = FirebaseDatabase.getInstance().getReference().child("Model");
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabaseUser = FirebaseDatabase.getInstance().getReference().child("User").child(mCurrentUser.getUid());
        mProgress = new ProgressDialog(this);

        setContentView(R.layout.activity_model_single_edit);
        /*Ojo La posicion afecta a la ejecucion*/
        ButterKnife.bind(this);
       // ButterKnife.bind(this, view);

       // mModelSingleImageSelected = (ImageButton) findViewById(R.id.imageSelect);
        mModelSingleImageSelected = (ImageButton) findViewById(R.id.imageSelect);
        mModelSingleImageSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent galleryIntet = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntet.setType("image/*");
                startActivityForResult(galleryIntet, GALLERY_REQUEST);
            }
        });


        mModel_key = getIntent().getExtras().getString("Model_Key");
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Model");
        mDatabaseRegister = FirebaseDatabase.getInstance().getReference();

        mDatabase.child(mModel_key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                System.out.println("Valores");
                System.out.println(dataSnapshot.getValue());
                // Model model = dataSnapshot.getValue(Model.class);
                Model model = dataSnapshot.getValue(Model.class);
                //System.out.println(model);
                if(model!= null) {

                    nameModelEditText.setText(model.getModel_Name());
                    costPriceEditText.setText(model.getCost_Price().toString());
                    salePriceEditText.setText(model.getSale_Price().toString());
                    descriptionModelEditText.setText(model.getModel_Description());
                    inStockEditText.setText(model.getIn_Stock().toString());
                    //mImageUri = Uri.parse((model.getImage_Url()).toString());
                    url_Current_Image = Uri.parse((model.getImage_Url()).toString());
                    Picasso.with(ModelSingleEditActivity.this).load(model.getImage_Url()).into(mModelSingleImageSelected);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());

            }


        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onValidationSucceeded() {
         //Log.d("Validations:", "Yay! we got it right!");
        //Toast.makeText(ModelsFragment.this.getActivity(), "Yay! we got it right!", Toast.LENGTH_SHORT).show();
        mProgress.setMessage(getString(R.string.messageUpdating));
        //mProgress.show();

        final String modelName = nameModelEditText.getText().toString();
        final Double costPrice = Double.parseDouble(costPriceEditText.getText().toString());
        final Double salePrice = Double.parseDouble(salePriceEditText.getText().toString());
        final String descriptionModel = descriptionModelEditText.getText().toString();
        final Integer inStock = Integer.parseInt(inStockEditText.getText().toString());
        //Imagen Actual
        //final String urlCurrentImage = url_Current_Image.toString();
        //if (mImageUri != null)
        if (state == true) {
            mProgress.show();
            Log.d("Estado", "Nueva Imagen");
            Log.d("Mi Image:", mImageUri.getLastPathSegment());
            StorageReference filepath = mStorage.child("Model_Images").child(mImageUri.getLastPathSegment());
            Log.d("Direccion Guardado", filepath.toString());

            filepath.putFile(mImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    final Uri dowloadUrl = taskSnapshot.getDownloadUrl();
                    //final DatabaseReference newModel = mDatabase.push();

                    //Antes de eso debemos eliminar la imagen antigua
                           /*
                            Log.d("Error","Origen: "+ mStorage);
                            Log.d("Error","URL: "+ url_Current_Image.getLastPathSegment());
                            */
                    // Create a storage reference from our app
                    StorageReference storageRef = mStorage;
                    // Create a reference to the file to delete
                    StorageReference desertRef = storageRef.child(url_Current_Image.getLastPathSegment());
                    // Delete the file
                    desertRef.delete().addOnSuccessListener(new OnSuccessListener() {
                        @Override
                        public void onSuccess(Object o) {
                            mProgress.dismiss();
                            ModelSingleEditActivity.this.finish();
                            Intent intent = new Intent(ModelSingleEditActivity.this, NavigationDrawerActivity.class);
                            startActivity(intent);

                            /*Ahora si podemos actualizar*/
                            mDatabaseUser.addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {

                                    final String image_Url = dowloadUrl.toString();
                                    final String user_id = mCurrentUser.getUid();

                                    Model model = new Model(modelName, costPrice, salePrice, descriptionModel, inStock,  image_Url,  user_id);
                                    Map<String, Object> modelValues = model.toMap();
                                    Map<String, Object> childUpdates = new HashMap<>();
                                    childUpdates.put("/Model/" + mModel_key, modelValues);
                                    //childUpdates.put("/user-models/" + userId + "/" + key, modelValues);
                                    mDatabaseRegister.updateChildren(childUpdates);
                                    //Toast.makeText(ModelSingleEditActivity.this, R.string.messageSuccessfullyRegistered, Toast.LENGTH_SHORT).show();
                                   // mProgress.dismiss();
                                }
                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                            /*
                            mProgress.dismiss();
                            ModelSingleEditActivity.this.finish();
                            Intent intent = new Intent(ModelSingleEditActivity.this, NavigationDrawerActivity.class);
                            startActivity(intent);

                            */

                        }
                                 /*
                                @Override
                                public void onSuccess(Void aVoid) {
                                    // File deleted successfully
                                }
                                */
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            Log.e("Error", "Uh-oh, an error occurred!");
                            Log.e("Error", "Ovidio revisar errorrr OJO!");
                            //progressDialog.dismiss();
                        }
                    });








                }
            });

        }
        else
        {
            //Aumentamos un timer 2 por que actualizar muy rapido y no se nota el progressDialog
            messageProgressDialog();

            //ModelSingleEditActivity.this.finish();
            /*
            Intent intent = new Intent(ModelSingleEditActivity.this, NavigationDrawerActivity.class);
            startActivity(intent);
            */

            //ActiveProgressDialog();
            /*
            mProgress.setProgress(100);
            mProgress.show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    mProgress.dismiss();
                }}, 10000);
                */
            //mProgress.setMax(100);
            //mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);

            /*
            mProgress.setIndeterminate(true);
            mProgress.setCancelable(false);
            mProgress.show();

            //long delayInMillis = 5000;
            long delayInMillis = 5000;

            Timer timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    mProgress.dismiss();
                }
            }, delayInMillis);
            */


            //En caso que la imagen no se halla cambiado
            Log.d("Estado", "Entrando en Version sin Renovar Imagen");
             mDatabaseUser.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    final String image_Url = url_Current_Image.toString();
                    final String user_id = mCurrentUser.getUid();

                    Model model = new Model(modelName, costPrice, salePrice, descriptionModel, inStock,  image_Url,  user_id);
                    Map<String, Object> modelValues = model.toMap();

                    Map<String, Object> childUpdates = new HashMap<>();
                    childUpdates.put("/Model/" + mModel_key, modelValues);
                    //childUpdates.put("/user-models/" + userId + "/" + key, modelValues);
                    mDatabaseRegister.updateChildren(childUpdates);
                    //Toast.makeText(ModelSingleEditActivity.this, R.string.messageSuccessfullyRegistered, Toast.LENGTH_SHORT).show();


                   // mProgress.dismiss();

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        }


    }

    public  void messageProgressDialog()
    {

        //http://stackoverflow.com/questions/14445745/android-close-dialog-after-5-seconds
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setMessage(getString(R.string.messageUpdating));
        dialog.show();
        final Timer timer2 = new Timer();
        timer2.schedule(new TimerTask() {
            public void run() {
                dialog.dismiss();
                timer2.cancel(); //this will cancel the timer of the system
                ModelSingleEditActivity.this.finish();
                Intent intent = new Intent(ModelSingleEditActivity.this, NavigationDrawerActivity.class);
                startActivity(intent);

            }
        }, 2000); // the timer will count 2 seconds....
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_REQUEST && resultCode == RESULT_OK) {
            state=true;
            mImageUri = data.getData();
            mModelSingleImageSelected.setImageURI(mImageUri);
        }
        else {
            state=false;
        }
    }

    @OnClick(R.id.saveModelButton)
    public void saveModelButton() {

        validator.validate();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(ModelSingleEditActivity.this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(ModelSingleEditActivity.this, message, Toast.LENGTH_LONG).show();
            }
        }

    }
}
