package com.example.simon.salesmanagementapp;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.simon.salesmanagementapp.model.Detail_Sale;
import com.example.simon.salesmanagementapp.model.Model;
import com.example.simon.salesmanagementapp.model.Sale;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class SalesFragment extends Fragment {

    private ImageButton btn_date;
    Calendar dateTime = Calendar.getInstance();
    private TextView mTextViedDate;
    ImageButton mShowDialog;
    private Query mQueryCurrentUser;
    private Query mQueryCurrentModel;
    private FirebaseAuth mAuth;
    private View view=null;

    private View mView=null;
    private View mViewFinalize=null;

    private TableLayout stk;

    //@NotEmpty
    private EditText quantity_DialogAlertEditText;

    private TextView totalGarment;

    private TextView amountReceivable;
    Validator validator;
    private ArrayList<Model> models;
    private Double sumTotalImporte = 0.0;

    Double recibeCash = 0.0;
    Double cambio = 0.0;
    Double amount_Receivable= 0.0;
    //private EditText recibeCash_DialogAlertEditText;

    //private Double recibeCash= 0.0;
    DatabaseReference mDatabase;
    DatabaseReference mDatabaseRegSale;
    DatabaseReference mDB;



    private String modelIdSpinner;
    String modelName;
    Double salePrice ;
    private Integer modelInStock;

    String currentUserId;
    Integer modelInStockSearch;

    Model currentModelSearch;

    public Integer stockCurrent = 0;



    public SalesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //final View view = inflater.inflate(R.layout.fragment_sales, container, false);
         view = inflater.inflate(R.layout.fragment_sales, container, false);
        //quantity_EditText=(EditText)view.findViewById(R.id.quantityEditText);
        totalGarment = (TextView)view.findViewById(R.id.total_Garment);
        amountReceivable = (TextView)view.findViewById(R.id.amount_receivable);
        mTextViedDate = (TextView) view.findViewById(R.id.txt_date_time);
        btn_date = (ImageButton) view.findViewById(R.id.dateSelectedButton);
        updateTextLabel();
        DateFormat formatDateTime = DateFormat.getDateTimeInstance();

        btn_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateDate();
            }
        });
        mShowDialog = (ImageButton) view.findViewById(R.id.btnShowDialog);


        mShowDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
                mView = getActivity().getLayoutInflater().inflate(R.layout.dialog_spinner,null);
                mBuilder.setTitle(R.string.lblSelectionDialogAlert);

                //Apuntamos a la vista
                final Spinner mSpinner = (Spinner) mView.findViewById(R.id.spinnerDialog);


                mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
                {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,int position, long id)
                    {
                        final Spinner spinner = (Spinner)mView.findViewById(R.id.spinnerDialog);
                        Model mod =(Model)spinner.getSelectedItem();
                         modelName=mod.getModel_Name();
                         salePrice = mod.getSale_Price();
                        // modelName=mod.getModel_Name();
                        //this will give position of item in spinner
                         //Log.i("Id:::", parent.getItemAtPosition(position).toString());
                      //  mQueryCurrentModel = mDB.child("Model").orderByChild("Model_Name").equalTo(modelName);
                        //
                        mQueryCurrentModel = mDB.child("Model").orderByChild("Model_Name").equalTo(modelName);


                        //OP 2 Falla
                        //mQueryCurrentModel = mDB.child("Model").child(modelName);

                        //http://stackoverflow.com/questions/38232140/how-to-get-the-key-from-the-value-in-firebase
                        mQueryCurrentModel.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                //OP 1
                                for (DataSnapshot modelSearchSnapshot: dataSnapshot.getChildren()) {
                                    Model currentModel = modelSearchSnapshot.getValue(Model.class);
                                    //modelId = modelSearchSnapshot.getKey().toString();
                                    //inStockDialogAlertTextView
                                    TextView inStockTextView = (TextView)mView.findViewById(R.id.inStockDialogAlertTextView);
                                    TextView preciUnitTextView = (TextView)mView.findViewById(R.id.priceUnitDialogAlertTextView);

                                    modelIdSpinner= modelSearchSnapshot.getKey().toString();
                                    Log.i("Id:::", modelIdSpinner);
                                    Log.i("Modelo:::", currentModel.getModel_Name().toString());
                                    Log.i("InStock:::", currentModel.getIn_Stock().toString());
                                    modelInStock=currentModel.getIn_Stock();
                                    inStockTextView.setText(modelInStock.toString());
                                    preciUnitTextView.setText(currentModel.getSale_Price().toString());

                                }
                                //OP 2
/*
                                  Log.i("Id Final:::", dataSnapshot.getKey().toString());
*/
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });


                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // TODO Auto-generated method stub

                    }
                });

                 //Codigo Mejorado
                //Leemos los datos
                /*
                ArrayAdapter<String> adapter= new ArrayAdapter<String>(getActivity(),
                        android.R.layout.simple_spinner_item,
                        getResources().getStringArray(R.array.restauranList));
                        */
                ArrayAdapter<Model> adapter= new ArrayAdapter<Model>(getActivity(),
                        android.R.layout.simple_spinner_item,
                        models);

                //FirebaseRecyclerAdapter<Model,ModelsFragment.ModelViewHolder> firebaseRecyclerAdapter
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                //Cargamos los datos al Spinner
                mSpinner.setAdapter(adapter);


                //Damos los Botones
                mBuilder.setNegativeButton(R.string.cancelDialogAlert, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });


                mBuilder.setPositiveButton(R.string.confirmDialogAlert, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        EditText quantity = (EditText)mView.findViewById(R.id.quantityDialogAlertEditText);
                        if (quantity.getText().toString().length() == 0) {
                            //quantity.requestFocus();
                            //quantity.setError("quantity is required!");
                            Toast.makeText(SalesFragment.this.getActivity(),"quantity is required!", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Integer cantidad = Integer.parseInt(quantity.getText().toString());
                            if (cantidad == 0) {
                                Toast.makeText(SalesFragment.this.getActivity(),"quantity mayor a 0", Toast.LENGTH_SHORT).show();

                            }
                            else {
                                if(cantidad<=modelInStock) {
                                    //mProgress.setMessage(getString(R.string.messageRegistering));
                                    dialog.dismiss();

                                    leerDatos();

                                }
                                else {
                                    Toast.makeText(SalesFragment.this.getActivity(),getString(R.string.insufficientQuantityMessage)+"\n"+getString(R.string.quantityAvailableMessage)+modelInStock, Toast.LENGTH_SHORT).show();
                                    //quantity.requestFocus();
                                    //quantity.setError("Cantidad Disponible"+modelInStock+"Cantidad Insuficiente en Stock");
                                }
                            }
                            // Ocultar teclado virtual
                            /*
                            InputMethodManager imm =
                                    (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                            */
                        }

                    }
                });

                mBuilder.setView(mView);
                AlertDialog dialog = mBuilder.create();
                dialog.show();
            }
        });

         //Probando con datos del usuario
        mAuth = FirebaseAuth.getInstance();
        //final String currentUserId= mAuth.getCurrentUser().getUid();
         currentUserId= mAuth.getCurrentUser().getUid();

        //DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("Model");
         mDatabase = FirebaseDatabase.getInstance().getReference().child("Model");
        mDatabaseRegSale = FirebaseDatabase.getInstance().getReference();
        mDB= FirebaseDatabase.getInstance().getReference();
        ///Consulta
        mQueryCurrentUser = mDatabase.orderByChild("User_Id").equalTo(currentUserId);
         // LLenar Spinerrr
         //http://stackoverflow.com/questions/38492827/how-to-get-a-string-list-from-firebase-to-fill-a-spinner
          mQueryCurrentUser.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Is better to use a List, because you don't know the size
                // of the iterator returned by dataSnapshot.getChildren() to
                // initialize the array
                //final ArrayList<Model> models = new ArrayList<Model>();
                 models = new ArrayList<Model>();

                //models.add("Selecte Model..");
                for (DataSnapshot modelSnapshot: dataSnapshot.getChildren()) {

                    Model currentModel = modelSnapshot.getValue(Model.class);
                    Log.i("Id:::", modelSnapshot.getKey().toString());
                    ///Sufriendo como recuperar el Id de manera eficiente
                    //http://stackoverflow.com/questions/37094631/get-the-pushed-id-for-specific-value-in-firebase-android
                    // String key = getRef(position).getKey();//Revisar Model Fragment
                    Log.i("Modelo:::", currentModel.getModel_Name().toString());
                    //currentModel.getClass().getKey
                    //Log.i("Description:::", currentModel.getModel_Description().toString());
                    models.add(currentModel);
                    //Log.i("THE_Model:::", currentModel.getModel_Name().toString());
                   // Log.i("modelDescription:::", currentModel.getModel_Description().toString());
                 }



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

        //https://developer.android.com/reference/android/widget/Button.html
        final Button buttonCancel = (Button) view.findViewById(R.id.cancelButton);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(SalesFragment.this.getActivity(),"Cancelando Venta", Toast.LENGTH_SHORT).show();

                //funcionReducir("-K_VaOfTrdNVR2SQfRqv",35);
                AlertDialog alertDialog = cancelSaleDialog();
                alertDialog.show();
                /*

                */



            }
        });
        final Button buttonFinalize = (Button) view.findViewById(R.id.nextButton);
        buttonFinalize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Double amount_Receivable = Double.parseDouble(amountReceivable.getText().toString());

                 amount_Receivable = Double.parseDouble(amountReceivable.getText().toString());
                if(amount_Receivable == 0)
                {
                    Toast.makeText(SalesFragment.this.getActivity(), R.string.selectItemMessage, Toast.LENGTH_SHORT).show();

                }
                  else {
                    AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
                    mViewFinalize = getActivity().getLayoutInflater().inflate(R.layout.dialog_finalize, null);
                    mBuilder.setTitle(R.string.finalizeDialogAlert);

                    final TextView txtTotalImporte = (TextView) mViewFinalize.findViewById(R.id.totalImporteDialogAlertTextView);
                    txtTotalImporte.setText(sumTotalImporte.toString());

                    final Double totalImporte = Double.parseDouble(sumTotalImporte.toString());
                    final TextView change = (TextView) mViewFinalize.findViewById(R.id.changeCashDialogAlertEditText);
                    final EditText recibeCash_DialogAlertEditText = (EditText) mViewFinalize.findViewById(R.id.recibeCashDialogAlertEditText);
                    // recibeCash_DialogAlertEditText = (EditText) mViewFinalize.findViewById(R.id.recibeCashDialogAlertEditText);

//                final Double recibeCash= 0.0;
                    change.setText("0.0");
                    recibeCash_DialogAlertEditText.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                            //recibeCash = Double.parseDouble(recibeCash_DialogAlertEditText.toString());
                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            if (recibeCash_DialogAlertEditText.getText().toString().length() == 0) {
                                //Toast.makeText(SalesFragment.this.getActivity(),"Debe ingresar un valor mayor a 0", Toast.LENGTH_SHORT).show();
                                change.setText("0.0");
                                recibeCash_DialogAlertEditText.requestFocus();
                                recibeCash_DialogAlertEditText.setError(getString(R.string.fieldIsRequiered));
                            } else {
                                Double recibeCashVal = Double.parseDouble(recibeCash_DialogAlertEditText.getText().toString());
                                if (recibeCashVal == 0) {
                                    //Toast.makeText(SalesFragment.this.getActivity(),"No puede ingresar 0", Toast.LENGTH_SHORT).show();
                                    change.setText("0.0");
                                    recibeCash_DialogAlertEditText.requestFocus();
                                    recibeCash_DialogAlertEditText.setError(getString(R.string.youCannotEnter0));
                                } else {
                                    /*
                                    Double recibeCash = Double.parseDouble(recibeCash_DialogAlertEditText.getText().toString());
                                    Double cambio = recibeCash - totalImporte;
                                    */
                                     recibeCash = Double.parseDouble(recibeCash_DialogAlertEditText.getText().toString());
                                     cambio = recibeCash - totalImporte;
                                    change.setText(cambio.toString());
                                }
                            }

                        }
                    });
                    Button confirm = (Button) mViewFinalize.findViewById(R.id.confirmButtonDialogFinalize);
                    Button cancel = (Button) mViewFinalize.findViewById(R.id.cancelButtonDialogFinalize);

                    mBuilder.setView(mViewFinalize);
                    final AlertDialog dialog = mBuilder.create();

                    confirm.setOnClickListener(
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    // Confirm

                                    //Total por venta: "+amount_Receivable+" Recibido: "+recibeCash
                                    if(recibeCash>=amount_Receivable) {


                                        String fechaVenta = mTextViedDate.getText().toString();
                                        // Toast.makeText(SalesFragment.this.getActivity(), "sale_date: " + fechaVenta + "Total por venta: " + amount_Receivable + " Recibido: " + recibeCash + " cambio: " + cambio, Toast.LENGTH_SHORT).show();
                                        // Toast.makeText(SalesFragment.this.getActivity(),"Guardando Venta!", Toast.LENGTH_SHORT).show();
                                        final String keySale = mDatabaseRegSale.child("Sales").push().getKey();
                                        Sale saleCurrent = new Sale(fechaVenta, amount_Receivable,recibeCash,cambio,currentUserId);
                                        mDatabaseRegSale.child("Sale").child(keySale).setValue(saleCurrent);
                                        // mDatabase.child("users").child(userId).setValue(user);
                                        leerItems(keySale);

                                        messageProgressDialog();
                                        dialog.dismiss();
                                    }
                                    else
                                    {
                                        Toast.makeText(SalesFragment.this.getActivity(), R.string.errorReceivedMoneyMessage, Toast.LENGTH_SHORT).show();

                                    }
                                }
                            }
                    );

                    cancel.setOnClickListener(
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    // Cancel...
                                   dialog.dismiss();
                                    //mBuilder.dismiss();
                                }
                            }

                    );
                    dialog.show();
                }



            }
        });

        init();
        return view;

    }
    //getActivity().onBackPressed();
    public AlertDialog cancelSaleDialog() {

        //getActivity es para el caso de fragmentos
        //AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());/
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());


        builder.setTitle(R.string.exitSaleDialogAlert)
                .setMessage(R.string.areYouSureCancelSaleDialogAlert)
                .setPositiveButton(R.string.messageOpNo,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                .setNegativeButton(R.string.messageOpYes,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getActivity().finish();
                                Intent intent = new Intent(SalesFragment.this.getActivity(), NavigationDrawerActivity.class);
                                startActivity(intent);
                            }
                        });

        return builder.create();
    }

    public  void messageProgressDialog()
    {

        //http://stackoverflow.com/questions/14445745/android-close-dialog-after-5-seconds
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        //dialog.setMessage(getString(R.string.messageUpdating));
        dialog.setMessage(getString(R.string.savingSaleMessage));

        dialog.show();
        final Timer timer2 = new Timer();
        timer2.schedule(new TimerTask() {
            public void run() {
                dialog.dismiss();
                timer2.cancel(); //this will cancel the timer of the system
                //RegisterInventoryByModelActivity.this.finish();
                Intent intent = new Intent(SalesFragment.this.getActivity(), NavigationDrawerActivity.class);
                startActivity(intent);


            }
        }, 2000); // the timer will count 2 seconds....
    }
    public void leerDatos()
    {

         //Aumentamos una Fila
        TableRow tbrow = new TableRow(getActivity());

        TextView t1v = new TextView(getActivity());
        t1v.setText(modelName);

        t1v.setTextColor(Color.BLACK);
        t1v.setGravity(Gravity.CENTER);
        tbrow.addView(t1v);

        TextView t2v = new TextView(getActivity());
        quantity_DialogAlertEditText=(EditText)mView.findViewById(R.id.quantityDialogAlertEditText);
        Integer Cantidad=Integer.parseInt(quantity_DialogAlertEditText.getText().toString());
        t2v.setText(Cantidad.toString());
        quantity_DialogAlertEditText.setText("");

        t2v.setTextColor(Color.BLACK);
        t2v.setGravity(Gravity.CENTER);
        //Cantidad/
        tbrow.addView(t2v);

        TextView t3v = new TextView(getActivity());
        //Precio de Venta//
        t3v.setText(salePrice.toString());


        t3v.setTextColor(Color.BLACK);
        t3v.setGravity(Gravity.CENTER);
        tbrow.addView(t3v);

        TextView t4v = new TextView(getActivity());

        Double totalImporte=Cantidad * salePrice;
        t4v.setText(totalImporte.toString());


        t4v.setTextColor(Color.BLACK);
        t4v.setGravity(Gravity.CENTER);
        tbrow.addView(t4v);


        TextView t5v = new TextView(getActivity());
        t5v.setText(modelIdSpinner);

        t5v.setTextColor(Color.BLACK);
        t5v.setGravity(Gravity.CENTER);

        t5v.setVisibility(View.INVISIBLE);
        t5v.setVisibility(View.GONE);


        tbrow.addView(t5v);


        TextView t6v = new TextView(getActivity());
        t6v.setText(modelInStock.toString());

        t6v.setTextColor(Color.BLACK);
        t6v.setGravity(Gravity.CENTER);

        t6v.setVisibility(View.INVISIBLE);
        t6v.setVisibility(View.GONE);

        tbrow.addView(t6v);

        stk.addView(tbrow);


        ContarGarment();
        ContarTotalImporte();

    }
    public void ContarGarment()
    {
        Integer TotalRows=stk.getChildCount();
        int i = 1;
        int sum=0;
        while (i < TotalRows )
        {
            String valor=((TextView)((TableRow)stk.getChildAt(i)).getChildAt(1)).getText().toString();
            //Toast.makeText(SalesFragment.this.getActivity(),"Valor "+ valor, Toast.LENGTH_SHORT).show();
            sum = sum + Integer.parseInt(valor);
            i++;
        }
        //Toast.makeText(SalesFragment.this.getActivity(),"Valor "+ sum, Toast.LENGTH_SHORT).show();
        //TextView totalGarment=(TextView)total_Garment

        totalGarment.setText(String.valueOf(sum));
    }
    public void ContarTotalImporte()
    {
        int TotalRows=stk.getChildCount();
        int i = 1;

        sumTotalImporte = 0.0;

        while (i < TotalRows )
        {
            String valor=((TextView)((TableRow)stk.getChildAt(i)).getChildAt(3)).getText().toString();
            //Toast.makeText(SalesFragment.this.getActivity(),"Valor "+ valor, Toast.LENGTH_SHORT).show();
            sumTotalImporte = sumTotalImporte + Double.parseDouble(valor);
            i++;
        }
        //Toast.makeText(SalesFragment.this.getActivity(),"Valor "+ sum, Toast.LENGTH_SHORT).show();
        //TextView totalGarment=(TextView)total_Garment

       amountReceivable.setText(String.valueOf(sumTotalImporte));
    }

    public void leerItems(String IdSale)
    {
        int TotalRows=stk.getChildCount();
        int i = 1;
        //sumTotalImporte = 0.0;
        //final String keyDetailSale = mDatabaseRegSale.child("Detail_Sale").push().getKey();
        while (i < TotalRows )
        {
            String modeloItem=((TextView)((TableRow)stk.getChildAt(i)).getChildAt(0)).getText().toString();
            final Integer cantidadItem=Integer.parseInt(((TextView)((TableRow)stk.getChildAt(i)).getChildAt(1)).getText().toString());
            Double precioItem=Double.parseDouble(((TextView)((TableRow)stk.getChildAt(i)).getChildAt(2)).getText().toString());
            Double totalItem=  cantidadItem*precioItem;
            //Id Modelo
            String modeloIdItem=((TextView)((TableRow)stk.getChildAt(i)).getChildAt(4)).getText().toString();
            //Stock Actual Del Modelo
            final Integer stockModelItem=Integer.parseInt(((TextView)((TableRow)stk.getChildAt(i)).getChildAt(5)).getText().toString());

            //Toast.makeText(SalesFragment.this.getActivity(),"Valor "+ valor, Toast.LENGTH_SHORT).show();
            //sumTotalImporte = sumTotalImporte + Double.parseDouble(valor);
            //Toast.makeText(SalesFragment.this.getActivity(),"modeloIdItem: "+modeloIdItem+" modeloItem "+ modeloItem+"cantidadItem"+"totalItem:"+totalItem, Toast.LENGTH_SHORT).show();

            //Debe crear un Nuevo keyDetail
            String keyDetailSale = mDatabaseRegSale.child("Detail_Sale").push().getKey();
            Detail_Sale detailSaleCurrent = new Detail_Sale(modeloIdItem, cantidadItem,precioItem,totalItem,IdSale);
            mDatabaseRegSale.child("Detail_Sale").child(keyDetailSale).setValue(detailSaleCurrent);

            Integer NewStockModelItem=stockModelItem-cantidadItem;
            funcionReducir(modeloIdItem,NewStockModelItem);

            //Ahora Actualizar mi Bd reduciendo el Stock
            i++;
        }

    }
    public void funcionReducir(String KeyModel,Integer New_stock)
    {
        DatabaseReference  Ref = mDB.child("Model").child(KeyModel);
        Map<String, Object> updates = new HashMap<String, Object>();
        updates.put("In_Stock",New_stock );
        Ref.updateChildren(updates);
    }


    public Integer returnStock(String modeloIdItem)
    {
        stockCurrent = 0;
        mDB.child("Model").child(modeloIdItem).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Model model = dataSnapshot.getValue(Model.class);
                if(model!= null) {

                    currentModelSearch =dataSnapshot.getValue(Model.class);
                    String KeyModel=dataSnapshot.getKey().toString();
                    Log.i("OJOOO::","Sufriendo Mucho");
                    Log.i("Key Model:::",KeyModel);
                    Log.i("Modelo:::", currentModelSearch.getModel_Name().toString());
                    Log.i("Stock Atual:::", currentModelSearch.getIn_Stock().toString());
                    //Integer inStockActual=currentModelSearch.getIn_Stock();
                    stockCurrent = currentModelSearch.getIn_Stock();

                    //Ahora Reducir su stock segun su idModel
                   //Integer New_stock = modelInStockSearch - cantidadItem;
                    //Log.i("New_stock:::", New_stock.toString());
                    //funcionReducir(KeyModel,New_stock);
                   // return stockCurrent;

                }
               // return stockCurrent;

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());

            }



        });

        return stockCurrent;
    }


    public void recuperar(String modeloItem,final Integer cantidadItem)
    {
        Query mQueryModelSearch = mDB.child("Model").orderByChild("Model_Name").equalTo(modeloItem);
        mQueryModelSearch.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot modelSnapshot : dataSnapshot.getChildren())
                {
                    currentModelSearch = modelSnapshot.getValue(Model.class);
                    String KeyModel=modelSnapshot.getKey().toString();
                    Log.i("Key Model:::",KeyModel);
                    Log.i("Modelo:::", currentModelSearch.getModel_Name().toString());
                    Log.i("Stock Atual:::", currentModelSearch.getIn_Stock().toString());
                    //Integer inStockActual=currentModelSearch.getIn_Stock();
                    modelInStockSearch = currentModelSearch.getIn_Stock();
                    //Ahora Reducir su stock segun su idModel
                    Integer New_stock = modelInStockSearch - cantidadItem;
                    Log.i("New_stock:::", New_stock.toString());

                    //funcionReducir(KeyModel,New_stock);

                    //OP 1 Funcion Raro / Incrementa cada Segundo
                        /*

                        DatabaseReference hopperRef = mDB.child("Model").child(KeyModel);
                        Map<String, Object> hopperUpdates = new HashMap<String, Object>();
                        hopperUpdates.put("In_Stock",New_stock );
                        hopperRef.updateChildren(hopperUpdates);
                        */


                    //OP 2
                    //Model model = new Model(modelName, costPrice, salePrice, descriptionModel, New_stock, image_Url, user_id);
                    //Map<String, Object> modelValues = model.toMap();
                    //Map<String, Object> childUpdates = new HashMap<>();
                    //childUpdates.put("/Model/" + mModel_key, modelValues);

                    //childUpdates.put("/Inventory_Models/" +key_Inventary + "/" + user_id, modelValues);
                    //childUpdates.put("/Inventory_By_Models/" +key_Inventary + "/" + user_id, Income_stock);
                    //mDatabase.updateChildren(childUpdates);



                    // OP 3
                    //String taskId = KeyModel;


                       /*
                        Model model = currentModelSearch;
                        Map<String, Object> modelValues = model.toMap();
                        Map<String, Object> childUpdates = new HashMap<>();
                        childUpdates.put("/Model/" + KeyModel, modelValues);
                        */


                    //http://stackoverflow.com/questions/33315353/update-specific-keys-using-firebase-for-android
                    //FirebaseDatabase.getInstance().getReference().child("tasks").child(KeyModel).updateChildren(result);

                        /*
                        Map<String, Object> modelValues = currentModelSearch.toMap();
                        //Map<String, Object> childUpdates = new HashMap<>();
                        //HashMap<String, Object> result = new HashMap<>();
                        modelValues.put("In_Stock", New_stock );
                        FirebaseDatabase.getInstance().getReference().child("Model").child(KeyModel).updateChildren(modelValues);
                        */



                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }


    public void init() {
        //OJO Crear TableRow Dinamico
        //http://stackoverflow.com/questions/18207470/adding-table-rows-dynamically-in-android
        //TableLayout stk = (TableLayout) view.findViewById(R.id.table_main);
        stk = (TableLayout) view.findViewById(R.id.table_main);
        TableRow tbrow0 = new TableRow(getActivity());

        TextView tv0 = new TextView(getActivity());
        tv0.setText(R.string.lblModeTitle);
        tv0.setTextColor(Color.BLACK);
        tv0.setTypeface(null, Typeface.BOLD_ITALIC);
        //http://stackoverflow.com/questions/13264794/font-size-of-textview-in-android-application-changes-on-changing-font-size-from
        //Also note that if the textSize is set in code
        // Use textView.setTextSize(X) interprets the number (X) as SP.
        // Use textView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, X) to set values in dp.
        tv0.setTextSize(17);
        tbrow0.addView(tv0);

        TextView tv1 = new TextView(getActivity());
        tv1.setText(R.string.lblQuantityTitle);
        tv1.setTextColor(Color.BLACK);
        tv1.setTypeface(null, Typeface.BOLD_ITALIC);
        tv1.setTextSize(17);
        tbrow0.addView(tv1);

        TextView tv2 = new TextView(getActivity());
        tv2.setText(R.string.lblPriceTitle);
        tv2.setTextColor(Color.BLACK);
        tv2.setTypeface(null, Typeface.BOLD_ITALIC);
        tv2.setTextSize(17);
        tbrow0.addView(tv2);

        TextView tv3 = new TextView(getActivity());
        tv3.setText(R.string.lblAmountTitle);
        tv3.setTextColor(Color.BLACK);
        //tv3.setTextAppearance(getApplicationContext(), R.style.boldText);
        //tv3.setTextSize("18sp");
        //http://stackoverflow.com/questions/4630440/how-to-change-a-textviews-style-at-runtime
        //tv3.setBackgroundResource(R.color.normalTextViewColor);
        //tv3.setTypeface(null, Typeface.BOLD_ITALIC);
        //tv3.setTypeface(Typeface.create("sans-serif", Typeface.BOLD_ITALIC));
        //http://stackoverflow.com/questions/12128331/how-to-change-fontfamily-of-textview-in-android
        tv3.setTypeface(Typeface.SANS_SERIF, Typeface.BOLD_ITALIC);
        tv3.setTextSize(17);
        tbrow0.addView(tv3);


        TextView tv4 = new TextView(getActivity());
        tv4.setText(" Id");
        tv4.setTextColor(Color.BLACK);
        tv4.setTypeface(Typeface.SANS_SERIF, Typeface.BOLD_ITALIC);
        tv4.setTextSize(17);

        tv4.setVisibility(View.INVISIBLE);
        tv4.setVisibility(View.GONE);

        tbrow0.addView(tv4);


        TextView tv5 = new TextView(getActivity());
        tv5.setText(" Stock");
        tv5.setTextColor(Color.BLACK);
        tv5.setTypeface(Typeface.SANS_SERIF, Typeface.BOLD_ITALIC);
        tv5.setTextSize(17);

        tv5.setVisibility(View.INVISIBLE);
        tv5.setVisibility(View.GONE);

        tbrow0.addView(tv5);


        stk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(SalesFragment.this.getActivity(), "Toco el Table Row", Toast.LENGTH_SHORT).show();
                /*TableRow tr1=(TableRow)view;
                TextView tv1= (TextView)tr1.getChildAt(0);
                Toast.makeText(SalesFragment.this.getActivity(),tv1.getText().toString(),Toast.LENGTH_SHORT).show();
                */

            }
        });
        stk.addView(tbrow0);


    }
    private void updateDate() {
        new DatePickerDialog(SalesFragment.this.getActivity(), d, dateTime.get(Calendar.YEAR), dateTime.get(Calendar.MONTH), dateTime.get(Calendar.DAY_OF_MONTH)).show();
    }
    DatePickerDialog.OnDateSetListener d = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            dateTime.set(Calendar.YEAR, year);
            dateTime.set(Calendar.MONTH, monthOfYear);
            dateTime.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateTextLabel();
        }
    };
    private void updateTextLabel() {
        //Calendar c = Calendar.getInstance();
        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");//En Sismte
        //SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        //SimpleDateFormat sdf = new SimpleDateFormat("dd- MMMM- yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MMM/yyyy");
        //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String strDate = sdf.format(dateTime.getTime());
        //mTextViedDate.setText(dateTime.getTime().toString());
         mTextViedDate.setText(strDate.toString());
    }

}
