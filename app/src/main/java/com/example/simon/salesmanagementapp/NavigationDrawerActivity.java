package com.example.simon.salesmanagementapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class NavigationDrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    //firebase auth object
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private DatabaseReference mDatabaseUsers;

    //view objects
    //private TextView textViewUserEmail;
    //private Button buttonLogout;
    private FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                        */

                fab.setVisibility(View.GONE);
                RegisterModelFragment registerModelFragment= new RegisterModelFragment();
                FragmentManager manager = getSupportFragmentManager();
                manager.beginTransaction().replace(
                        R.id.content_navigation_drawer,
                        registerModelFragment,
                        registerModelFragment.getTag()).commit();
                //Title en el Action Bar
                 getSupportActionBar().setTitle(R.string.titleActionBarAddModel);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        mDatabaseUsers = FirebaseDatabase.getInstance().getReference().child("User");
        //For activate offline mode
        mDatabaseUsers.keepSynced(true);

        //initializing firebase authentication object
        mAuth = FirebaseAuth.getInstance();


        /*
        //Login 01 Funciona
        //if the user is not logged in
        //that means current user will return null

        if(mAuth.getCurrentUser() == null){
            //closing this activity
             finish();
            //starting login activity
             startActivity(new Intent(this, LoginActivity.class));
        }
        //getting current user
        FirebaseUser user = mAuth.getCurrentUser();
        Toast.makeText(this, "Welcome: " + user.getEmail(), Toast.LENGTH_LONG).show();

        //Enabled Offline
       // mDatabaseUser = FirebaseDatabase.getInstance().getReference().child("User");
       // mDatabaseUser.keepSynced(true);
      */


        //Login 02 Mejorando
        mAuthListener = new FirebaseAuth.AuthStateListener(){

            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                 FirebaseUser user = firebaseAuth.getCurrentUser();
                //if the user is not logged in
                //that means current user will return null
                if (user == null) {
                     //checkUserExist();


                    //that means user is already logged in
                    //so close this activity
                    finish();
                    //and open profile activity
                    //startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    Intent loginIntent=new Intent(NavigationDrawerActivity.this,LoginActivity.class);
                              loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(loginIntent);



                }
                else {


                    //getting current user
                  //  FirebaseUser user = mAuth.getCurrentUser();
                    //Toast.makeText(NavigationDrawerActivity.this, getString(R.string.toastWelcome) + user.getEmail(), Toast.LENGTH_LONG).show();
                }

            }
        };



       /*
       //Login Basic usando SharedPreferences
        SharedPreferences preferences = getSharedPreferences(getString(R.string.app_name),MODE_PRIVATE);
        // int user_id = preferences.getInt("user_id",0);


        String  user_id = preferences.getString("user_id","");
        if (user_id.isEmpty())
        {

            //Usuario no Logueado
            Intent intent=new Intent(this,LoginActivity.class);
            startActivity(intent);

        }
        else {

            Toast.makeText(this, "user_id: " + user_id, Toast.LENGTH_LONG).show();

        }
        */


//        fab.setVisibility(View.GONE);
    }

    @Override
    public void onStart() {
        super.onStart();
        checkUserExist();
        mAuth.addAuthStateListener(mAuthListener);
        /*
        fab.setVisibility(View.VISIBLE);

        ModelsFragment modelsFragment= new ModelsFragment();
        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction().replace(
                R.id.content_navigation_drawer,
                modelsFragment,
                modelsFragment.getTag()).commit();
        //Title en el Action Bar
         //getSupportActionBar().setTitle("Models");

        //fab.setVisibility(View.GONE);
         // ...
         */
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
        // ...
    }

    private void checkUserExist() {

        if(mAuth.getCurrentUser()!=null)
        {

            final String user_id = mAuth.getCurrentUser().getUid();
            mDatabaseUsers.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    /*
                    if (!dataSnapshot.hasChild(user_id)) {
                        //Intent setupIntent=new Intent(getApplicationContext(),SetupActivity.class);
                        Intent setupIntent = new Intent(NavigationDrawerActivity.this, SetupActivity.class);
                        //Probaremos modo Offline

                        //Intent setupIntent=new Intent(NavigationDrawerActivity.this,NavigationDrawerActivity.class);

                        setupIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(setupIntent);
                    }
                    */
                    if (!dataSnapshot.hasChild(user_id)) {
                      //  Toast.makeText(NavigationDrawerActivity.this,user_id, Toast.LENGTH_SHORT).show();
                                // Toast.makeText(NavigationDrawerActivity.this, (dataSnapshot.hasChild(user_id)).toString(), Toast.LENGTH_SHORT).show();
                        Toast.makeText(NavigationDrawerActivity.this, R.string.toastYouNeedToSetupYourAccount, Toast.LENGTH_SHORT).show();
                        Intent setupIntent = new Intent(NavigationDrawerActivity.this, SetupActivity.class);
                        setupIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(setupIntent);


                    }
                    else {

/*
                        //Funciona pero falla al apagar la pantalla se pierde lo que se estaba trabajando mejor investigar
                        fab.setVisibility(View.VISIBLE);

                        ModelsFragment modelsFragment= new ModelsFragment();
                        FragmentManager manager = getSupportFragmentManager();
                        manager.beginTransaction().replace(
                                R.id.content_navigation_drawer,
                                modelsFragment,
                                modelsFragment.getTag()).commit();
                        //Title en el Action Bar
                        getSupportActionBar().setTitle("Models");

*/
                        //fab.setVisibility(View.GONE);
                        /*
                        fab.setVisibility(View.VISIBLE);

                        ModelsFragment modelsFragment= new ModelsFragment();
                        FragmentManager manager = getSupportFragmentManager();
                        manager.beginTransaction().replace(
                                R.id.content_navigation_drawer,
                                modelsFragment,
                                modelsFragment.getTag()).commit();
                        //Title en el Action Bar
                        getSupportActionBar().setTitle("Model");
                        */
                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation_drawer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        /*
        if (id == R.id.nav_camera) {
            // Handle the camera action
            Toast.makeText(this,"Camera",Toast.LENGTH_SHORT).show();

            CameraFragment cameraFragment= new CameraFragment();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(
                     R.id.content_navigation_drawer,
                    cameraFragment,
                    cameraFragment.getTag()).commit();

        }
        else
        */
        if (id == R.id.nav_Inventory) {
            fab.setVisibility(View.GONE);
            //Toast.makeText(this,"Inventory",Toast.LENGTH_SHORT).show();

            fab.setVisibility(View.VISIBLE);

            InventoryByModelFragment inventoryByModelFragment= new InventoryByModelFragment();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(
                    R.id.content_navigation_drawer,
                    inventoryByModelFragment,
                    inventoryByModelFragment.getTag()).commit();
            //Title en el Action Bar
            getSupportActionBar().setTitle(item.getTitle());

        }
        else if (id == R.id.nav_Models) {
             //Toast.makeText(this,"Models",Toast.LENGTH_SHORT).show();
            /*
            RegisterModelFragment registerModelFragment= new RegisterModelFragment();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(
                    R.id.content_navigation_drawer,
                    registerModelFragment,
                    registerModelFragment.getTag()).commit();
*/
            fab.setVisibility(View.VISIBLE);

            ModelsFragment modelsFragment= new ModelsFragment();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(
                    R.id.content_navigation_drawer,
                    modelsFragment,
                    modelsFragment.getTag()).commit();
            //Title en el Action Bar
            getSupportActionBar().setTitle(item.getTitle());
}
        else if (id == R.id.nav_Sales) {

            fab.setVisibility(View.GONE);
            //Toast.makeText(this,"Inventory",Toast.LENGTH_SHORT).show();

            SalesFragment salesFragment= new SalesFragment();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(
                    R.id.content_navigation_drawer,
                    salesFragment,
                    salesFragment.getTag()).commit();
            //Title en el Action Bar
            getSupportActionBar().setTitle(item.getTitle());
            //drawerLayout.closeDrawer(GravityCompat.START);
/*
   Revisar el cerrado
//http://desarrollador-android.com/material-design/desarrollo-material-design/pautas-desarrollo/navigation-drawer-con-fragments/
            DrawerLayout drawer1 = (DrawerLayout) findViewById(R.id.drawer_layout);
            drawer1.closeDrawer(GravityCompat.START);
            */


        }
        else if (id == R.id.nav_Reports) {
            fab.setVisibility(View.GONE);
            //Toast.makeText(this,"Reports",Toast.LENGTH_SHORT).show();
            ReportFragment reportFragment= new ReportFragment();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(
                    R.id.content_navigation_drawer,
                    reportFragment,
                    reportFragment.getTag()).commit();
            //Title en el Action Bar
            getSupportActionBar().setTitle(item.getTitle());


        } else if (id == R.id.nav_About) {
            fab.setVisibility(View.GONE);
            //Toast.makeText(this,"Reports",Toast.LENGTH_SHORT).show();
            AboutFragment aboutFragment= new AboutFragment();
            FragmentManager manager = getSupportFragmentManager();
            manager.beginTransaction().replace(
                    R.id.content_navigation_drawer,
                    aboutFragment,
                    aboutFragment.getTag()).commit();
            //Title en el Action Bar
            getSupportActionBar().setTitle(item.getTitle());

        }
        else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_Logout) {

            /*
            SharedPreferences preferences=getSharedPreferences(getString(R.string.app_name),//Nombre Archivo
                    Context.MODE_PRIVATE);//Solo Nuestra App Puede Ver Esto
            SharedPreferences.Editor editor=preferences.edit();//Abrir el archivo para escritura
            editor.remove("user_id");//Elimina el username

            //editor.remove("username");//Elimina el username
            editor.commit();//Guarda Cambios
            Toast.makeText(this,getString(R.string.lbl_Logout),Toast.LENGTH_SHORT).show();

            Intent intent=new Intent(this, NavigationDrawerActivity.class);
            startActivity(intent);
            */

            AlertDialog alertDialog = cancelSaleDialog();
            alertDialog.show();
/*

            //logging out the user
            mAuth.signOut();
            //closing activity
            finish();
            //starting login activity
            startActivity(new Intent(this, LoginActivity.class));
            */

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public AlertDialog cancelSaleDialog() {

        //getActivity es para el caso de fragmentos
        //AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());/
        AlertDialog.Builder builder = new AlertDialog.Builder(this);


        builder.setTitle(R.string.lblLogout)
                .setMessage(R.string.lblAreyouSureLogout)
                .setPositiveButton(R.string.messageOpNo,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        })
                .setNegativeButton(R.string.messageOpYes,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                //this.finish();
                               // Intent intent = new Intent(SalesFragment.this.getActivity(), NavigationDrawerActivity.class);
                                //startActivity(intent);
                                //logging out the user
                                mAuth.signOut();
                                //closing activity
                                finish();
                                //starting login activity
                               startActivity(new Intent(NavigationDrawerActivity.this, LoginActivity.class));

                                /*
                                Intent loginIntent=new Intent(NavigationDrawerActivity.this,LoginActivity.class);
                                loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(loginIntent);
                                */
                            }
                        });

        return builder.create();
    }
}
