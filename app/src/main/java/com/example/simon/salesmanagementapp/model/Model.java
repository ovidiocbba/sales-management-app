package com.example.simon.salesmanagementapp.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Simon on 30/10/2016.
 */
@IgnoreExtraProperties
public class Model {
    private String Model_Name;
    private Double Cost_Price;
    private Double Sale_Price;
    private String Model_Description;
    private Integer In_Stock;
    private String Image_Url;
    private String User_Id;

  public Model(){

    }
    public Model(String model_Name, Double cost_Price, Double sale_Price, String model_Description, Integer in_Stock, String image_Url, String user_id) {
        Model_Name = model_Name;
        Cost_Price = cost_Price;
        Sale_Price = sale_Price;
        Model_Description = model_Description;
        In_Stock = in_Stock;
        Image_Url = image_Url;
        User_Id = user_id;
    }
    public String getUser_Id() {
        return User_Id;
    }

    public void setUser_Id(String user_Id) {
        User_Id = user_Id;
    }
    public String getModel_Name() {
        return Model_Name;
    }

    public void setModel_Name(String model_Name) {
        Model_Name = model_Name;
    }

    public Double getCost_Price() {
        return Cost_Price;
    }

    public void setCost_Price(Double cost_Price) {
        Cost_Price = cost_Price;
    }

    public Double getSale_Price() {
        return Sale_Price;
    }

    public void setSale_Price(Double sale_Price) {
        Sale_Price = sale_Price;
    }

    public String getModel_Description() {
        return Model_Description;
    }

    public void setModel_Description(String model_Description) {
        Model_Description = model_Description;
    }

    public Integer getIn_Stock() {
        return In_Stock;
    }

    public void setIn_Stock(Integer in_Stock) {
        In_Stock = in_Stock;
    }

    public String getImage_Url() {
        return Image_Url;
    }

    public void setImage_Url(String image_Url) {
        Image_Url = image_Url;
    }



    public String toString(){
        return Model_Name;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("Model_Name",Model_Name);
        result.put("Cost_Price" ,Cost_Price);
        result.put("Sale_Price",Sale_Price);
        result.put("Model_Description",Model_Description);
        result.put("In_Stock",In_Stock);
        result.put("Image_Url",Image_Url);
        result.put("User_Id",User_Id);

        return result;
    }
}
