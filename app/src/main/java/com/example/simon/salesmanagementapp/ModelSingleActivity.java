package com.example.simon.salesmanagementapp;

import android.support.v7.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.simon.salesmanagementapp.model.Model;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

public class ModelSingleActivity extends AppCompatActivity {

    private static final String TAG ="MoldeSingleActivity" ;
    private String mModel_key = null;
    private DatabaseReference mDatabase;
    private ImageView mModelSingleImage;
    private TextView mModelSingleName;
    private TextView mModelSingleSalePrice;
    private TextView mModelSingleCostPrice;
    private TextView mModelSingleDescription;
    private TextView mModelSingleInStock;

    private FirebaseAuth mAuth;
    private Button mSingleModelDeleteBtn;
    private String model_UserId;
    private ProgressDialog progressDialog;
    private boolean isFinished=false;
    private StorageReference mStorageImage;
    private Uri mImageUri = null;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_model_single);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.lblDetail);
        progressDialog = new ProgressDialog(this);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Model");
        mModel_key = getIntent().getExtras().getString("Model_Key");
        //Toast.makeText(ModelSingleActivity.this,model_key,Toast.LENGTH_SHORT).show();
        /*
        model_image
                model_name
        model_price_sale
        */
        mModelSingleName =(TextView)findViewById(R.id.model_name);
        mModelSingleImage = (ImageView)findViewById(R.id.model_image);
        mModelSingleSalePrice = (TextView)findViewById(R.id.model_sale_price);
        mModelSingleCostPrice = (TextView)findViewById(R.id.model_cost_price);
        mModelSingleDescription= (TextView)findViewById(R.id.model_description);
        mModelSingleInStock=(TextView)findViewById(R.id.model_in_stock);

        mSingleModelDeleteBtn = (Button)findViewById(R.id.deleteModelButton);

        //mStorageImage = FirebaseStorage.getInstance().getReference().child("Model_Images");
        mStorageImage = FirebaseStorage.getInstance().getReference();



        mDatabase.child(mModel_key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                //System.out.println(dataSnapshot.getValue());
                Model model = dataSnapshot.getValue(Model.class);
                //System.out.println(model);


                /*
                String model_name = model.getModel_Name();
                Double model_sale_Price = (Double)model.getSale_Price();
                String model_image=model.getImage_Url();

               String model_name = (String) dataSnapshot.child("Model_Name").getValue();
               String model_sale_Price = (String) dataSnapshot.child("Sale_Price").getValue();
               String model_image=(String) dataSnapshot.child("Image_Url").getValue();
               //String model_user_id=(String) dataSnapshot.child("UserId").getValue();
               */

                //https://firebase.google.com/docs/database/admin/retrieve-data?hl=en
                if(model!= null) {
                    model_UserId = model.getUser_Id();
                    mModelSingleName.setText(model.getModel_Name());
//                    mModelSingleSalePrice.setText("Sale Price: "+model.getSale_Price().toString()+".-");
                    mModelSingleSalePrice.setText(model.getSale_Price().toString()+".-");
                    //mModelSingleCostPrice.setText("Cost Price: "+model.getCost_Price().toString()+".-");

                    //Double(total).toString()
                    mModelSingleCostPrice.setText(String.valueOf(model.getCost_Price())+".-");
                    mModelSingleInStock.setText(String.valueOf(model.getIn_Stock())+".-");
                    mModelSingleDescription.setText(model.getModel_Description());
                    mImageUri = Uri.parse((model.getImage_Url()).toString());
                    Picasso.with(ModelSingleActivity.this).load(model.getImage_Url()).into(mModelSingleImage);
                }
                /*
                newModel.child("Model_Name").setValue(nameModel);
                newModel.child("Cost_Price").setValue(costPrice);
                newModel.child("Sale_Price").setValue(salePrice);
                newModel.child("Model_Description").setValue(descriptionModel);
                newModel.child("In_Stock").setValue(inStock);
                newModel.child("Image_Url").setValue(dowloadUrl.toString());
                newModel.child("User_Id").setValue(mCurrentUser.getUid());
                */
                 //Toast.makeText(ModelSingleActivity.this,mAuth.getCurrentUser().getUid()+" "+model_UserId,Toast.LENGTH_SHORT).show();
                //if(mAuth.getCurrentUser().getUid()== model_UserId)
                if(mAuth.getCurrentUser().getUid().equals(model_UserId))
                {
                   // mSingleModelDeleteBtn.setVisibility(View.VISIBLE);

                }
//
//                if(isFinished){
//                    System.out.println("LLegue al Removedor");
//                    mDatabase.removeEventListener(this);
//                }

            }


            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("The read failed: " + databaseError.getCode());

            }



        });
    }
    /*
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
    */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_model_detail,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //onBackPressed();

        int id = item.getItemId();


        if (id == android.R.id.home) {
            onBackPressed();
        }
        if (id == R.id.action_edit) {
            //Toast.makeText(ModelSingleActivity.this,"Presiono Editar",Toast.LENGTH_SHORT).show();
           /*
            Intent singleModelIntent = new Intent(getContext(), ModelSingleActivity.class);
            singleModelIntent.putExtra("Model_Key", model_key);
            startActivity(singleModelIntent);
           */

            Intent editIntent = new Intent(ModelSingleActivity.this, ModelSingleEditActivity.class);
            editIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            editIntent.putExtra("Model_Key", mModel_key);
            startActivity(editIntent);

        }
        if (id == R.id.action_delete) {
            //Toast.makeText(ModelSingleActivity.this,"Presiono Delete",Toast.LENGTH_SHORT).show();
            //deleteOnClick();
            AlertDialog alertDialog = deleteModelDialog();
            alertDialog.show();
            /*
            AlertDialog alertDialog=new AlertDialog.Builder(ModelSingleActivity.this).create();
            alertDialog.setTitle("Prueba");
            //alertDialog.setTitle("Title: "+p.getTitle());
            alertDialog.setMessage("Hello");
            alertDialog.setButton(DialogInterface.BUTTON_NEUTRAL,"Cerrar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            alertDialog.show();
            */
        }

        return super.onOptionsItemSelected(item);

    }
/*
    public static String getFileNameByUri(Context context, Uri uri)
    {
        String fileName="unknown";//default fileName
        Uri filePathUri = uri;
        if (uri.getScheme().toString().compareTo("content")==0)
        {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            if (cursor.moveToFirst())
            {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);//Instead of "MediaStore.Images.Media.DATA" can be used "_data"
                filePathUri = Uri.parse(cursor.getString(column_index));
                fileName = filePathUri.getLastPathSegment().toString();
            }
        }
        else if (uri.getScheme().compareTo("file")==0)
        {
            fileName = filePathUri.getLastPathSegment().toString();
        }
        else
        {
            fileName = fileName+"_"+filePathUri.getLastPathSegment();
        }
        return fileName;
    }
 */
/*/
public class DialogoConfirmacion extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {



        AlertDialog.Builder builder =
                new AlertDialog.Builder(getActivity());

        builder.setMessage("¿Confirma la acción seleccionada?")
                .setTitle("Confirmacion")
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener()  {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.i("Dialogos", "Confirmacion Aceptada.");
                        dialog.cancel();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Log.i("Dialogos", "Confirmacion Cancelada.");
                        dialog.cancel();
                    }
                });

        return builder.create();
    }
}*/
public AlertDialog deleteModelDialog() {

    //getActivity es para el caso de fragmentos
    //AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());/
    AlertDialog.Builder builder = new AlertDialog.Builder(ModelSingleActivity.this);


    builder.setTitle(R.string.messageDeleteModel)
            .setMessage(R.string.messageDoYouReallyWantToDeleteModel)
            .setPositiveButton(R.string.messageOpNo,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            //listener.onPossitiveButtonClick();
                            //deleteOnClick();
                        }
                    })
            .setNegativeButton(R.string.messageOpYes,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                           // listener.onNegativeButtonClick();
                            //Toast.makeText(ModelSingleActivity.this,"No Se puede Eliminar",Toast.LENGTH_SHORT).show();
                            deleteOnClick();
                        }
                    });

    return builder.create();
}

    public void deleteOnClick() {
        //Toast.makeText(ModelSingleActivity.this,"Presiono Eliminar",Toast.LENGTH_SHORT).show();

        //displaying a progress dialog
         progressDialog.setMessage(getString(R.string.messageDeleting));
         progressDialog.show();

        //Eliminar Directo

        /*
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.popBackStack(fragmentManager.getBackStackEntryAt(fragmentManager.getBackStackEntryCount()-2).getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        */

        //DatabaseReference ref = FirebaseDatabase.getInstance().ref();

        Query modelQuery = mDatabase.child(mModel_key);

        modelQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot modelSnapshot: dataSnapshot.getChildren()) {

                    //modelSnapshot.getRef().removeValue();
                    mDatabase.child(mModel_key).removeValue();
                    Log.d(TAG,"Origen: "+ mStorageImage);
                    Log.d(TAG,"URL: "+ mImageUri.getLastPathSegment());
                    // Create a storage reference from our app
                    StorageReference storageRef = mStorageImage;
                    // Create a reference to the file to delete
                    StorageReference desertRef = storageRef.child(mImageUri.getLastPathSegment());
                   // Log.d(TAG,"URL Final Revisar: "+ desertRef.toString());
                    //Toast.makeText(ModelSingleActivity.this,desertRef.toString(),Toast.LENGTH_SHORT).show();
                    // Delete the file
                    desertRef.delete().addOnSuccessListener(new OnSuccessListener() {
                        @Override
                        public void onSuccess(Object o) {

                            progressDialog.dismiss();
                            Intent mainIntent = new Intent(ModelSingleActivity.this, NavigationDrawerActivity.class);
                            mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(mainIntent);

                        }

                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            // Uh-oh, an error occurred!
                            Log.e(TAG, "Uh-oh, an error occurred!");
                            Log.e(TAG, "Ovidio revisar errorrr OJO!");
                            progressDialog.dismiss();
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled", databaseError.toException());
            }
        });


       // DatabaseReference ref = FirebaseDatabase.getInstance().ref();

       // mDatabase.child(model_UserId).setValue();

        //Toast.makeText(ModelSingleActivity.this,mDatabase.toString()+" ID "+ mModel_key,Toast.LENGTH_SHORT).show();

       // mDatabase.child(mModel_key).removeValue();
        /*

        Query deleteModelQuery = mDatabase;
        //Toast.makeText(ModelSingleActivity.this,(mDatabase.equalTo(model_UserId)).toString(),Toast.LENGTH_SHORT).show();


        deleteModelQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot modelSnapshot: dataSnapshot.getChildren()) {
                    modelSnapshot.getRef().removeValue();

                    Toast.makeText(ModelSingleActivity.this,"Intentando Eliminar",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, "onCancelled", databaseError.toException());
            }
        });
        */



    }



}
